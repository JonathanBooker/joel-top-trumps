﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.DTO
{
    public class User
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
    }
}
