﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.DTO
{
    public class Deck
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Card> Cards { get; set; }
        public List<DeckCategory> DeckCategories { get; set; }
    }
}
