﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.DTO
{
    public class DeckCategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid DeckId { get; set; }

    }
}
