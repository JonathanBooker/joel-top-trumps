﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.Enums
{
    public enum LoginResult
    {
        Success,
        Invalid
    }
}
