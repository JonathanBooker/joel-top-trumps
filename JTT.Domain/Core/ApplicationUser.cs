﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace JTT.Domain.Core
{
    public class ApplicationUser
    {
        [DisplayName(@"User ID")]
        public Guid UserId { get; set; }

        [DisplayName(@"First name")]
        public string FirstName { get; set; }

        [DisplayName(@"Last name")]
        public string Surname { get; set; }

        [DisplayName(@"Username")]
        public string Username { get; set; }

        [DisplayName(@"Role")]
        public string RoleName { get; set; }
    }
}
