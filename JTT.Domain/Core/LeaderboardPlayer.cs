﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.Core
{
    public class LeaderboardPlayer
    {
        public int Position { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public TimeSpan AverageGameDuration { get; set; }
        public TimeSpan TotalGameTime { get; set; }
    }
}
