﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.Domain.Core
{
    public class GameRecord
    {
        public TimeSpan GameDuration { get; set; }
        public Guid WinningPlayerId { get; set; }
        public Guid LosingPlayerId { get; set; }
        public Guid DeckId { get; set; }
    }
}
