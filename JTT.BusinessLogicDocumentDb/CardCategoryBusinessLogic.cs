﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Interfaces;
using JTT.UnitOfWorkDocumentDb.Classes;

namespace JTT.BusinessLogicDocumentDb
{
    public class CardCategoryBusinessLogic : ICardCategoryBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public CardCategoryBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Domain.DTO.CardCategory> CreateCardCategory(List<Domain.DTO.CardCategory> domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                //Check that the Card Category Score and Deck Category Id are set
                if (domainObject.Any(c => c.Score < 0 || c.DeckCategoryId == Guid.Empty))
                {
                    throw new Exception("Unable to save card category - Validation failed");
                }

                var first = domainObject.FirstOrDefault();

                if (first != null)
                {
                    var deckId = first.Card.DeckId;
                    var deck = _unitOfWork.GetById<Deck>(deckId);
                    var card = deck.Cards.SingleOrDefault(c => c.CardId == first.CardId);

                    if (card != null)
                    {
                        card.CardCategories = domainObject.Select(da => new CardCategory
                        {
                            DeckCategoryName = da.DeckCategoryName,
                            Score = da.Score
                        }).ToList();

                        _unitOfWork.Update(deck);
                    }
                }

                return domainObject;
            }
            catch (Exception ex)
            {
                //An error has occurred.
                throw new Exception("Unable to save card category");
            }
        }
    }
}
