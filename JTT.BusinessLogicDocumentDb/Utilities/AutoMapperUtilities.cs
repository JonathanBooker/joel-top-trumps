﻿using AutoMapper;
using System;
using JTT.Domain.DTO;

namespace JTT.BusinessLogicEntityFramework.Utilities
{
    public static class AutoMapperUtilities
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<UnitOfWorkDocumentDb.Classes.Card, Card>();
            Mapper.CreateMap<Card, UnitOfWorkDocumentDb.Classes.Card>();

            Mapper.CreateMap<UnitOfWorkDocumentDb.Classes.CardCategory, CardCategory>();
            Mapper.CreateMap<CardCategory, UnitOfWorkDocumentDb.Classes.CardCategory>();

            Mapper.CreateMap<UnitOfWorkDocumentDb.Classes.Deck, Deck>();
            Mapper.CreateMap<Deck, UnitOfWorkDocumentDb.Classes.Deck>();

            Mapper.CreateMap<UnitOfWorkDocumentDb.Classes.DeckCategory, DeckCategory>();
            Mapper.CreateMap<DeckCategory, UnitOfWorkDocumentDb.Classes.DeckCategory>();

            Mapper.CreateMap<UnitOfWorkDocumentDb.Classes.User, User>();
            Mapper.CreateMap<User, UnitOfWorkDocumentDb.Classes.User>();
        }
    }
}
