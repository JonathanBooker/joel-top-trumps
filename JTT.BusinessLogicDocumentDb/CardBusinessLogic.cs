﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Interfaces;
using JTT.UnitOfWorkDocumentDb.Classes;

namespace JTT.BusinessLogicDocumentDb
{
    public class CardBusinessLogic : ICardBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public CardBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Guid CreateCard(Domain.DTO.Card domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Card Name and Deck Id are set
                if (string.IsNullOrEmpty(domainObject.Name) || domainObject.DeckId == Guid.Empty)
                {
                    throw new Exception("Unable to save the card - Card Name or Guid not set");
                }

                var deck = _unitOfWork.GetById<Deck>(domainObject.DeckId);

                if (deck != null)
                {
                    var card = new Card
                    {
                        Name = domainObject.Name,
                        CardCategories = new List<CardCategory>(),
                        CardId = Guid.NewGuid()
                    };

                    foreach (var cardCategoryDto in domainObject.CardCategories)
                    {
                        var cardCategory = new CardCategory 
                        {
                            Id = cardCategoryDto.Id,
                            Score = cardCategoryDto.Score,
                            DeckCategoryId = cardCategoryDto.DeckCategoryId,
                            DeckCategoryName = cardCategoryDto.DeckCategoryName,
                            CardId = card.CardId,
                            Card = card
                        };
                        card.CardCategories.Add(cardCategory);
                    }

                    _unitOfWork.Update(deck);
                    return Guid.NewGuid();
                }

                return domainObject.Id;
            }
            catch (Exception ex)
            {
                //An error has occurred.
                throw new Exception("Unable to save the card");
            }
        }

        public List<Domain.DTO.Card> GetCardsForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<Deck>(deckId);

            if (deck != null)
            {
                return new List<Domain.DTO.Card>(deck.Cards.Select(c => new Domain.DTO.Card
                {
                    Id = c.CardId,
                    Name = c.Name,
                    CardCategories = c.CardCategories.Select(cc => new Domain.DTO.CardCategory
                    {
                        DeckCategoryName = cc.DeckCategoryName,
                        Score = cc.Score
                    }).ToList()
                })).ToList();
            }

            return new List<Domain.DTO.Card>();
        }

        public Domain.DTO.Card GetFirstCardForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<Deck>(deckId);
            if (deck != null && deck.Cards.Any())
            {
                var firstCard = deck.Cards.First();
                return new Domain.DTO.Card
                {
                    Id = firstCard.CardId,
                    Name = firstCard.Name,
                    CardCategories = firstCard.CardCategories.Select(cc => new Domain.DTO.CardCategory
                    {
                        DeckCategoryName = cc.DeckCategoryName,
                        Score = cc.Score
                    }).ToList()
                };
            }

            return null;
        }
    }
}
