﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.Core;
using JTT.Interfaces;
using JTT.UnitOfWorkDocumentDb.Classes;

namespace JTT.BusinessLogicDocumentDb
{
    public class GameBusinessLogic : IGameBusinessLogic
    {        
        private readonly IUnitOfWork _unitOfWork;

        public GameBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool RecordGameStats(Domain.Core.GameRecord gameRecord)
        {
            try
            {
                var winningUser = _unitOfWork.GetById<User>(gameRecord.WinningPlayerId);
                var losingPlayers = _unitOfWork.GetAll<User>().Where(u => gameRecord.LosingPlayerId == u.Id);

                var game = new Game
                {
                    Id = gameRecord.DeckId,
                    GameDuration = gameRecord.GameDuration.Ticks,
                    WinningUserId = winningUser.Id,
                    Users = new List<User>(losingPlayers.ToList())
                };

                _unitOfWork.Add(game);
                _unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<Domain.Core.LeaderboardPlayer> GetRankedUsers()
        {
            var users = _unitOfWork.GetAll<User>().ToList();

            foreach (var user in users)
            {
                if (user.Games == null)
                {
                    user.Games = new List<Game>();
                }
            }

            var leaderBoardUsers = users.Select(user => new LeaderboardPlayer
            {
                UserId = user.Id,
                Username = user.Username,
                //Wins = user.Games.Count(g => g.WinningUserId == user.Id),
                Losses = user.Games.Count,
                //TotalGameTime = user.Games.Sum(),
                //AverageGameDuration = user.AverageGameTime
            }).ToList();

            leaderBoardUsers = leaderBoardUsers.OrderByDescending(lp => lp.Wins).ToList();

            for (int counter = 0; counter < leaderBoardUsers.Count; counter++)
            {
                var u = leaderBoardUsers[counter];
                u.Position = counter + 1;
            }

            return leaderBoardUsers;
        }

        public List<Domain.Core.LeaderboardPlayer> GetSearchUsers(string searchText)
        {
            searchText = searchText.ToLower();

            var rankedUsers = GetRankedUsers();

            var searchUsers = new List<LeaderboardPlayer>();

            foreach (var leaderboardPlayer in rankedUsers)
            {
                if (leaderboardPlayer.Username.ToLower().Contains(searchText))
                {
                    searchUsers.Add(leaderboardPlayer);
                }
            }

            return searchUsers;
        }
    }
}
