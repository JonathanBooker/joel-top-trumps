﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.Core;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.UnitOfWorkDocumentDb.Classes;

namespace JTT.BusinessLogicDocumentDb
{
    public class LoginBusinessLogic : ILoginBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public LoginBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool ValidateUser(string username, string password)
        {
            return _unitOfWork.GetAll<User>().Any(sm => sm.Username == username &&
                password == EncryptionUtilities.Encrypt(password));
        }

        public ApplicationUser LoginUser(string username, string password)
        {
            var encryptedPassword = EncryptionUtilities.Encrypt(password);
            var user = _unitOfWork.GetAll<User>().SingleOrDefault(sm => sm.Username == username &&
                sm.Password == encryptedPassword);

            return user == null ? null : new ApplicationUser
            {
                UserId = user.Id,
                FirstName = user.Forname,
                Surname = user.Surname,
                Username = user.Username,
                RoleName = "StandardUser"
            };
        }

        public Domain.Enums.CreateUserResult CreateUser(Domain.DTO.User user, string password)
        {
            throw new NotImplementedException();
        }
    }
}
