﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;
using Deck = JTT.UnitOfWorkDocumentDb.Classes.Deck;

namespace JTT.BusinessLogicDocumentDb
{
    public class DeckCategoryBusinessLogic : IDeckCategoryBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeckCategoryBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Guid CreateDeckCategory(Domain.DTO.DeckCategory domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Card Name and Deck Id are set
                if (string.IsNullOrEmpty(domainObject.Name) || domainObject.DeckId == Guid.Empty)
                {
                    throw new Exception("Unable to save the card - Card Name or Guid not set");
                }

                var deck = _unitOfWork.GetById<Deck>(domainObject.DeckId);

                if (deck != null)
                {
                    deck.DeckCategories.Add(Mapper.Map<DeckCategory, UnitOfWorkDocumentDb.Classes.DeckCategory>(domainObject));
                }

                _unitOfWork.Update(deck);

                return domainObject.Id;
            }
            catch (Exception ex)
            {
                //An error has occurred.
                throw new Exception("Unable to save the card");
            }
        }

        public List<Domain.DTO.DeckCategory> GetDeckCategoriesForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<UnitOfWorkDocumentDb.Classes.Deck>(deckId);
            if (deck != null)
            {
                var categories = deck.DeckCategories;

                return categories.Select(c => Mapper.Map<DeckCategory>(c)).ToList();
            }

            return new List<DeckCategory>();
        }
    }
}
