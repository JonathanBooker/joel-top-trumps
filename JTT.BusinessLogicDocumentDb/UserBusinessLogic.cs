﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;

namespace JTT.BusinessLogicDocumentDb
{
    public class UserBusinessLogic : IUserBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public User GetUserById(Guid userId)
        {
            var user = _unitOfWork.GetById<User>(userId);

            return Mapper.Map<User>(user);
        }
    }
}
