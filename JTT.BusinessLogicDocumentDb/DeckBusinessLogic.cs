﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using JTT.Interfaces;
using JTT.UnitOfWorkDocumentDb.Classes;
using CardCategory = JTT.Domain.DTO.CardCategory;

namespace JTT.BusinessLogicDocumentDb
{
    public class DeckBusinessLogic : IDeckBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeckBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Domain.DTO.Deck GetById(Guid id)
        {
            var deck = _unitOfWork.GetById<Deck>(id);
            if (deck != null)
            {
                return Mapper.Map<Domain.DTO.Deck>(deck);
            }
            return null;
        }

        public List<Domain.DTO.Deck> GetAllDecks()
        {
            var decks = _unitOfWork.GetAll<Deck>().ToList();

            var deckDTOs = new List<Domain.DTO.Deck>();

            foreach (var deck in decks)
            {
                var deckDto = new Domain.DTO.Deck
                {
                    Id = deck.Id,
                    Name = deck.Name,
                    Cards = new List<Domain.DTO.Card>(),
                    DeckCategories = new List<Domain.DTO.DeckCategory>()
                };

                foreach (var deckCategory in deck.DeckCategories)
                {
                    var deckCategoryDTO = new Domain.DTO.DeckCategory
                    {
                        Id = deckCategory.Id,
                        Name = deckCategory.Name,
                        DeckId = deck.Id
                    };

                    foreach (var card in deck.Cards)
                    {
                        var cardDto = new Domain.DTO.Card
                        {
                            Id = card.CardId,
                            Name = card.Name,
                            DeckId = deck.Id,
                            CardCategories = new List<CardCategory>()
                        };

                        foreach (var cardCategory in card.CardCategories)
                        {
                            var cardCategoryDto = new Domain.DTO.CardCategory
                            {
                                Id = cardCategory.Id,
                                Score = cardCategory.Score,
                                DeckCategoryId = deckCategory.Id,
                                DeckCategoryName = deckCategory.Name,
                                CardId = card.CardId,
                                Card = cardDto
                            };
                            cardDto.CardCategories.Add(cardCategoryDto);
                        }
                        deckDto.Cards.Add(cardDto);
                    }
                    deckDto.DeckCategories.Add(deckCategoryDTO);
                }
                deckDTOs.Add(deckDto);
            }

            return deckDTOs;
        }

        public Guid CreateDeck(Domain.DTO.Deck domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Deck Name is set
                if (string.IsNullOrEmpty(domainObject.Name))
                {
                    throw new Exception("Unable to save the Deck - Deck Name is null or empty");
                }

                //Map the domain object to an Document DB object
                var deck = new Deck
                {
                    Id = domainObject.Id,
                    Name = domainObject.Name,
                    DeckCategories = new List<DeckCategory>(),
                    Cards = new List<Card>()
                };

                //Insert it in the database
                _unitOfWork.Add(deck);
                _unitOfWork.SaveChanges();

                return domainObject.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save the Deck", ex);
            }
        }
    }
}
