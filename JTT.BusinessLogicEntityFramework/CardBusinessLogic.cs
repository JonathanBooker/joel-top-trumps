﻿using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.BusinessLogicEntityFramework
{
    public class CardBusinessLogic : ICardBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public CardBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Guid CreateCard(Card domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Card Name and Deck Id are set
                if (string.IsNullOrEmpty(domainObject.Name) || domainObject.DeckId == Guid.Empty)
                {
                    throw new Exception("Unable to save the card - Card Name or Guid not set");
                }

                //Map the domain object to an Entity Framework object
                var obj = Mapper.Map<UnitOfWorkEf.Card>(domainObject);

                //Insert it in the database
                _unitOfWork.Add(obj);
                _unitOfWork.SaveChanges();
                return domainObject.Id;
            }
            catch (Exception ex)
            {
                //An error has occurred.
                throw new Exception("Unable to save the card");
            }
        }

        public List<Card> GetCardsForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<UnitOfWorkEf.Deck>(deckId);
            if (deck != null)
            {
                var cards = deck.Cards;
                
                return cards.Select(c => Mapper.Map<Card>(c)).ToList();
            }

            return new List<Card>();
        }

        public Card GetFirstCardForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<UnitOfWorkEf.Deck>(deckId);
            if (deck != null)
            {
                return Mapper.Map<Card>(deck.Cards.First());
            }

            return null;
        }
    }

}
