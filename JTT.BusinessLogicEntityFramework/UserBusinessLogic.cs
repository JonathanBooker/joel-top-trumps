﻿using System;
using AutoMapper;
using JTT.Interfaces;
using JTT.UnitOfWorkEf;

namespace JTT.BusinessLogicEntityFramework
{
    public class UserBusinessLogic : IUserBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Domain.DTO.User GetUserById(Guid userId)
        {
            var user = _unitOfWork.GetById<User>(userId);

            return Mapper.Map<Domain.DTO.User>(user);
        }
    }
}
