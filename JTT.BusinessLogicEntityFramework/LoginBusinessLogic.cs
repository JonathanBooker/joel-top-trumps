﻿using System.Linq;
using JTT.Domain.Core;
using JTT.Domain.Enums;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.UnitOfWorkEf;

namespace JTT.BusinessLogicEntityFramework
{
    public class LoginBusinessLogic : ILoginBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public LoginBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool ValidateUser(string username, string password)
        {
            return _unitOfWork.GetAll<User>().Any(sm => sm.Username == username && 
                password == EncryptionUtilities.Encrypt(password));
        }

        public ApplicationUser LoginUser(string username, string password)
        {
            var encryptedPassword = EncryptionUtilities.Encrypt(password);
            var user = _unitOfWork.GetAll<User>().SingleOrDefault(sm => sm.Username == username &&
                sm.Password == encryptedPassword);

            return user == null ? null : new ApplicationUser
            {
                UserId = user.Id,
                FirstName = user.Forename,
                Surname = user.Surname,
                Username = user.Username,
                RoleName = "StandardUser"
            };
        }

        public CreateUserResult CreateUser(Domain.DTO.User user, string password)
        {
            //    var dbUser = _unitOfWork.GetAll<User>().SingleOrDefault(u => u.Username.ToLower() == user.Username.ToLower());

            //    if (dbUser == null)
            //    {
            //        try
            //        {
            //            var standardRole = _unitOfWork.GetAll<Role>().First(r => r.Name == UserType.Standard.ToString());

            //            _unitOfWork.Add(new User
            //            {
            //                Username = user.Username,
            //                Password = EncryptDecryptUtilities.Encrypt(password),
            //                Firstname = user.Firstname,
            //                Surname = user.Surname,
            //                DateCreated = DateTime.Now,
            //                DateOfBirth = user.DateOfBirth,
            //                ContactNumber = user.ContactNumber,
            //                RoleId = standardRole.RoleId,
            //                Address = new Address
            //                {
            //                    LineOne = user.LineOne,
            //                    LineTwo = user.LineTwo,
            //                    City = user.City,
            //                    PostCode = user.PostCode
            //                }
            //            });
            //            _unitOfWork.SaveChanges();
            //        }
            //        catch (Exception exception)
            //        {
            //            return CreateUserResult.Failed;
            //        }
            //    }
            //    return CreateUserResult.DuplicateUser;

            return CreateUserResult.Successful;
        }
    }
}
