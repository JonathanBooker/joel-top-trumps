﻿using AutoMapper;
using System;
using JTT.UnitOfWorkEf;

namespace JTT.BusinessLogicEntityFramework.Utilities
{
    public static class AutoMapperUtilities
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Deck, Domain.DTO.Deck>();
            Mapper.CreateMap<Domain.DTO.Deck, Deck>();

            Mapper.CreateMap<DeckCategory, Domain.DTO.DeckCategory>();
            Mapper.CreateMap<Domain.DTO.DeckCategory, DeckCategory>();

            Mapper.CreateMap<Card, Domain.DTO.Card>();
            Mapper.CreateMap<Domain.DTO.Card, Card>();

            Mapper.CreateMap<CardCategory, Domain.DTO.CardCategory>()
                .ForMember(dest =>
                    dest.DeckCategoryName,
                    opt => opt.MapFrom(p => p.DeckCategory.Name));
            Mapper.CreateMap<Domain.DTO.CardCategory, CardCategory>();
            
            Mapper.CreateMap<User, Domain.DTO.User>();
        }
    }
}
