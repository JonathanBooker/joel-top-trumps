﻿using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.BusinessLogicEntityFramework
{
    public class DeckBusinessLogic : IDeckBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeckBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Deck GetById(Guid id)
        {
            var deck = _unitOfWork.GetById<UnitOfWorkEf.Deck>(id);
            if (deck != null)
            {
                return Mapper.Map<Deck>(deck);
            }
            return null;
        }

        public List<Deck> GetAllDecks()
        {
            var decks = _unitOfWork.GetAll<UnitOfWorkEf.Deck>().ToList();

            return decks.Select(d => Mapper.Map<Deck>(d)).ToList();
        }

        public Guid CreateDeck(Deck domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Deck Name is set
                if (string.IsNullOrEmpty(domainObject.Name))
                {
                    throw new Exception("Unable to save the Deck - Deck Name is null or empty");
                }

                //Map the domain object to an Entity Framework object
                var deck = Mapper.Map<UnitOfWorkEf.Deck>(domainObject);

                //Insert it in the database
                _unitOfWork.Add(deck);
                _unitOfWork.SaveChanges();
                return domainObject.Id;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save the Deck", ex);
            }
        }
    }
}
