﻿using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.BusinessLogicEntityFramework
{
    public class DeckCategoryBusinessLogic : IDeckCategoryBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public DeckCategoryBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Guid CreateDeckCategory(DeckCategory domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already
                if (domainObject.Id == Guid.Empty)
                {
                    //If it hasn't been set generate a new GUID
                    domainObject.Id = Guid.NewGuid();
                }

                //Check that the Category Name and Deck Id are set
                if (string.IsNullOrEmpty(domainObject.Name) || domainObject.DeckId == Guid.Empty)
                {
                    throw new Exception("Unable to save the deck category - Category Name or Deck Id are null");
                }

                //Map the domain object to an Entity Framework object
                var obj = Mapper.Map<UnitOfWorkEf.DeckCategory>(domainObject);

                //Insert it in the database
                _unitOfWork.Add(obj);
                _unitOfWork.SaveChanges();
                return domainObject.Id;
            }
            catch (Exception)
            {
                throw new Exception("Unable to save the deck category");
            }
        }

        public List<DeckCategory> GetDeckCategoriesForDeck(Guid deckId)
        {
            var deck = _unitOfWork.GetById<UnitOfWorkEf.Deck>(deckId);
            if (deck != null)
            {
                var categories = deck.DeckCategories;

                return categories.Select(c => Mapper.Map<DeckCategory>(c)).ToList();
            }

            return new List<DeckCategory>();
        }
    }
}
