﻿using AutoMapper;
using JTT.Domain.DTO;
using JTT.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.BusinessLogicEntityFramework
{
    public class CardCategoryBusinessLogic : ICardCategoryBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public CardCategoryBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<CardCategory> CreateCardCategory(List<CardCategory> domainObject)
        {
            try
            {
                //Check to see if the ID has been set on the domain object already

                //Check that the Card Category Score and Deck Category Id are set
                if (domainObject.Any(c => c.Score < 0 || c.DeckCategoryId == Guid.Empty))
                {
                    throw new Exception("Unable to save card category - Validation failed");
                }

                //Map the domain object to an Entity Framework object
                var obj = Mapper.Map<List<UnitOfWorkEf.CardCategory>>(domainObject);

                //Insert it in the database
                _unitOfWork.AddRange(obj);
                _unitOfWork.SaveChanges();
                return domainObject;
            }
            catch (Exception ex)
            {
                //An error has occurred.
                throw new Exception("Unable to save card category");
            }
        }
    }
}
