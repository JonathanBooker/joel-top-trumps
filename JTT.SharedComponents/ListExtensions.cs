﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace JTT.SharedComponents
{
    public static class ListExtensions
    {
        /// <summary>
        /// Based off of the StackOverflow solution of shuffling a list using a cryto-service over random which gives a better 'random'
        /// http://stackoverflow.com/questions/273313/randomize-a-listt-in-c-sharp
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            var cryptoServiceProvider = new RNGCryptoServiceProvider();
            int count = list.Count;
            while (count > 1)
            {
                var box = new byte[1];
                do cryptoServiceProvider.GetBytes(box);

                while (!(box[0] < count * (Byte.MaxValue / count)));

                int k = (box[0] % count);
                count--;
                T value = list[k];
                list[k] = list[count];
                list[count] = value;
            }
        }
    }
}
