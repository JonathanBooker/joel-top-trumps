﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace JTT.SharedComponents
{
    public class ConfigurationUtilities
    {
        public static string GetConfigurationSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
    }
}
