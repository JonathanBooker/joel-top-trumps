﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class User : TableEntity
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Forname { get; set; }
        public string Surname { get; set; }
        public string MobileNumber { get; set; }
        public List<Game> Games { get; set; }
    }
}
