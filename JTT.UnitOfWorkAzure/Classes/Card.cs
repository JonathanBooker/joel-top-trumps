﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class Card : TableEntity
    {
        public string CardId { get; set; }
        public string Name { get; set; }
        public string BlobImageId { get; set; }
        public List<CardCategory> CardCategories { get; set; }
    }
}
