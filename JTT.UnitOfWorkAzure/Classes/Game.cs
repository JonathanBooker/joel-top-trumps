﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class Game : TableEntity
    {
        public string GameId { get; set; }
        public string DeckName { get; set; }
        public int GameDuration { get; set; }
        public List<string> Users { get; set; }
        public string WinningUserId { get; set; }
    }
}
