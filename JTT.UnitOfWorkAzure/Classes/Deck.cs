﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class Deck : TableEntity
    {
        public string DeckId { get; set; }
        public string Name { get; set; }
        public List<DeckCategory> DeckCategories { get; set; }
        public List<Card> Cards { get; set; }
    }
}
