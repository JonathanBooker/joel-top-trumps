﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class DeckCategory : TableEntity
    {
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public bool HighestWins { get; set; }
    }
}
