﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Classes
{
    public class CardCategory : TableEntity
    {
        public string Name { get; set; }
        public int Score { get; set; }
    }
}
