﻿using System;
using System.Reflection;
using JTT.SharedComponents;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure.Utilities
{
    public class AzureUtilities<TEntity> where TEntity : class
    {
        private static string PartitionKey
        {
            get
            {
                return ConfigurationUtilities.GetConfigurationSetting("PartitionKey");
            }
        }

        dynamic CreateDTO(object a)
        {
            var dto = new TableEntityDto();
            object rowKey = null;

            Type t1 = a.GetType();
            Type t2 = dto.GetType();

            //now set all the entity properties
            foreach (PropertyInfo p in t1.GetProperties())
            {
                dto.TrySetMember(p.Name, p.GetValue(a, null) ?? "");
                if (IsId(p.Name))
                    rowKey = p.GetValue(a, null);
            }

            if (rowKey == null)
                rowKey = Guid.NewGuid();

            dto.RowKey = rowKey.ToString();
            dto.PartitionKey = PartitionKey;

            return dto;
        }

        public TEntity StripDto(DynamicTableEntity a)
        {
            var result = Activator.CreateInstance<TEntity>();
            var type = result.GetType();
            var dictionary = a.Properties;

            //For each property in the entity
            foreach (var propertyInfo in type.GetProperties())
            {
                //See if we have a corresponding property in the DTO
                foreach (var value in dictionary)
                {
                    if (propertyInfo.Name == value.Key)
                    {
                        propertyInfo.SetValue(result, GetValue(value.Value));
                    }
                }
            }

            return result;
        }

        private object GetValue(EntityProperty source)
        {
            switch (source.PropertyType)
            {
                case EdmType.Binary:
                    return source.BinaryValue;
                case EdmType.Boolean:
                    return source.BooleanValue;
                case EdmType.DateTime:
                    return source.DateTimeOffsetValue;
                case EdmType.Double:
                    return source.DoubleValue;
                case EdmType.Guid:
                    return source.GuidValue;
                case EdmType.Int32:
                    return source.Int32Value;
                case EdmType.Int64:
                    return source.Int64Value;
                case EdmType.String:
                    return source.StringValue;
                default: throw new TypeLoadException(string.Format("not supported edmType:{0}", source.PropertyType));
            }
        }

        private bool IsId(string candidate)
        {
            bool result = candidate.ToLower() == "id" || candidate.ToLower().Substring(candidate.Length - 2, 2) == "id";

            return result;
        }
    }
}
