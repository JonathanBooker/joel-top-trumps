﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.UnitOfWorkAzure.Utilities;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace JTT.UnitOfWorkAzure
{
    public class UnitOfWork : IUnitOfWork
    {
        private static string PartitionKey
        {
            get
            {
                return ConfigurationUtilities.GetConfigurationSetting("PartitionKey");
            }
        }

        public UnitOfWork()
        {  
        }

        private CloudTable GetCloudTable<T>()
        {
            //Get storage connection string. LOCAL: Storage Emulator. CLOUD: Azure Storage Account.
            var configurationSettingValue = "UseDevelopmentStorage=true";

            //Access cloud storage account. Uses connection string obtained above.
            var cloudStorageAccount = CloudStorageAccount.Parse(configurationSettingValue);

            //Create cloud table client. Provides access to Tables in your Storage Account 
            var cloudTableClient = cloudStorageAccount.CreateCloudTableClient();

            //Get Cloud Table for Message Table.
            var messageCloudTable = cloudTableClient.GetTableReference(typeof(T).Name);

            //Create Messages Table if it does not already exist. 
            messageCloudTable.CreateIfNotExists();

            //Output Messages Cloud Table object. Provides the means of accessing the Messages Table.
            return messageCloudTable;
        }

        public T GetById<T>(object id) where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();

            //Create a retrieval operation
            var retrieveOperation = TableOperation.Retrieve(PartitionKey, id as string);

            //Execute the retrieval
            var retrievalResult = cloudTable.Execute(retrieveOperation);

            //Get the item that was retrieved
            return retrievalResult.Result as T;
        }

        public T Add<T>(T entity) where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();

            //Create a insert operation
            var insertOperation = TableOperation.Insert(entity as TableEntity);

            //Execute the insert
            var insertionResult = cloudTable.Execute(insertOperation);

            //Get the item that was insert
            return insertionResult.Result as T;
        }

        public ICollection<T> AddRange<T>(ICollection<T> entity) where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();

            foreach (var item in entity)
            {
                //Create a insert operation
                var insertOperation = TableOperation.Insert(item as TableEntity);

                //Execute the insert
                var insertionResult = cloudTable.Execute(insertOperation);
            }

            return new List<T>();
        }

        public void Update<T>(T entity) where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();

            //Create a insert operation
            var replaceOperation = TableOperation.Replace(entity as TableEntity);

            //Execute the insert
            cloudTable.Execute(replaceOperation);
        }

        public void Remove<T>(T entity) where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();

            //Create a insert operation
            var deleteOperation = TableOperation.Delete(entity as TableEntity);

            //Execute the insert
            cloudTable.Execute(deleteOperation);
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            //Get the Cloud Table for the entity
            var cloudTable = GetCloudTable<T>();
            
            List<T> mappedList = new List<T>();
            var query = new TableQuery();
            var result = cloudTable.ExecuteQuery(query).ToList();

            var azureUtilities = new AzureUtilities<T>();

            foreach (var item in result)
            {
                mappedList.Add(azureUtilities.StripDto(item));
            }

            return mappedList.AsQueryable();
        }

        public bool HasBeenModified<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}
