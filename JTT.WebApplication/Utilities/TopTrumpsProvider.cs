﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using JTT.Domain.Core;
using JTT.Domain.Enums;
using JTT.Interfaces;

namespace JTT.WebApplication.Utilities
{
    public class TopTrumpsProvider
    {
        private static string Anonymous = "Anonymous";

        private readonly ILoginBusinessLogic _loginBusinessLogic;

        private static ApplicationUser ApplicationUser
        {
            get { return (ApplicationUser)HttpContext.Current.Session["ApplicationUser"]; }
            set { HttpContext.Current.Session["ApplicationUser"] = value; }
        }

        public static ApplicationUser GetLoggedinUser()
        {
            return ApplicationUser;
        }

        public static Guid GetLoggedInUserId()
        {
            return ApplicationUser.UserId;
        }

        public TopTrumpsProvider(ILoginBusinessLogic loginBusinessLogic)
        {
            _loginBusinessLogic = loginBusinessLogic;
        }

        public LoginResult LoginUser(string username, string password)
        {
            var result = _loginBusinessLogic.LoginUser(username, password);

            if (result != null)
            {
                ApplicationUser = result;

                var userIdentity = new GenericIdentity(result.Username);
                HttpContext.Current.User = new GenericPrincipal(userIdentity, new[] { "User" });

                FormsAuthentication.SetAuthCookie(username, false);

                return LoginResult.Success;
            }

            return LoginResult.Invalid;
        }

        public static void LoginAsAnonymous()
        {
            var userIdentity = new GenericIdentity(Anonymous);
            HttpContext.Current.User = new GenericPrincipal(userIdentity, new[] { "User" });

            ApplicationUser = new ApplicationUser
            {
                UserId = Guid.NewGuid(),
                FirstName = Anonymous,
                Surname = "User",
                Username = Anonymous,
                RoleName = "User"
            };
        }

        public static void LogoutUser()
        {
            var session = HttpContext.Current.Session;
            session.Clear();
            session.Abandon();

            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        public static bool IsUserCurrentlyLoggedIn()
        {
            return ApplicationUser != null;
        }

        public static bool IsUserAnonymous()
        {
            return ApplicationUser != null && ApplicationUser.Username == Anonymous;
        }
    }
}
