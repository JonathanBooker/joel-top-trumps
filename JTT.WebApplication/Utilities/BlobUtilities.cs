﻿using System;
using System.Collections.Generic;
using JTT.SharedComponents;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace JTT.WebApplication.Utilities
{
    public static class BlobUtilities
    {
        public static string GetBlobAddress(string deckName, Guid blobId)
        {
            //Retrieve the connection string
            string connStr = ConfigurationUtilities.GetConfigurationSetting("AzureConnectionString");

            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connStr);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            CloudBlobContainer container = blobClient.GetContainerReference(deckName.ToLower().Replace(" ", string.Empty));

            //Retrieve the blob
            CloudBlockBlob blob = container.GetBlockBlobReference(blobId.ToString());

            //Return the blobs uri
            return blob.Uri.ToString();
        }
    }
}
