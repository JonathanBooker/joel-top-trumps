﻿using System;
using System.Collections.Generic;
using System.Linq;
using EasyHttp.Http;
using JTT.SharedComponents;
using JTT.WebApplication.Models;

namespace JTT.WebApplication.Utilities
{
    public static class BitlyUtilities
    {
        public static string GetBitlyLink(string relativeUrl)
        {
            var http = new HttpClient();
            http.Request.Accept = HttpContentTypes.ApplicationJson;
            var response = http.Get(ConfigurationUtilities.GetConfigurationSetting("BitlyUrl"), new
            {
                access_token = ConfigurationUtilities.GetConfigurationSetting("BitlyApiKey"),
                longUrl = ConfigurationUtilities.GetConfigurationSetting("WebUrl") + relativeUrl,
                title = "Joel Top Trumps"
            });
            var bitlyResponse = response.StaticBody<BitlyResponse>();

            return bitlyResponse.Data.LinkSave.Link;
        }
    }
}
