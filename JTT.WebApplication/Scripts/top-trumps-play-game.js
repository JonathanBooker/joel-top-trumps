﻿//Set up various variables containing information about the game, etc
var userId;
var gameId;
var deckId;

//Variable used to determine if the initial load is over
var initialLoadComplete = false;

//Set the initial score cards for each player
var opposingPlayerScoreCard = $(".score.opposing-player");
var currentPlayerScoreCard = $(".score.current-player");

//Get the card containers for each card that will be shown
var opponentPlayerCardBack = $("#OpponentCardBack");
var currentPlayerCardBack = $("#CardBack");

//Disconnected timer
var disconnectedTimeout;

//Game load logic
$(function () {
    addCardPreload();

    $(window).bind("load", function () {
        removeCardPreload();
    });
});

//Utility functions
function addCardPreload() {
    $('.card').each(function () {
        $(this).addClass('preload');
    });
}

function removeCardPreload() {
    $('.card').each(function () {
        $(this).removeClass('preload');
    });
}


//Main game logic
function renderCurrentPlayerCard(gameCard, playMoveCallback) {
    //Get the current players next card and render it
    var renderedCard = renderCard(gameCard);

    setTimeout(function () {
        //Flip both cards back over so the next turn can take place
        flipPlayerCard(currentPlayerCardBack, false);
        flipPlayerCard(opponentPlayerCardBack, false);

        setTimeout(function () {
            //Set the current player card to their next card and then flip animate the card
            currentPlayerCardBack.html(renderedCard);

            //Remove the previous card from the card back of the opponent
            opponentPlayerCardBack.html('No cheating ;)');

            //Flip over the players card on a regular basis if the initial load is done
            if (initialLoadComplete) {
                flipPlayerCard(currentPlayerCardBack, true);
            }

            //This removes any transition on the cards on the first loading of it so it doesn't animate to the
            //size
            addCardPreload();
            removeCardPreload();

            //When a card category is clicked, notify the server of the selected move
            $(".playable .card-category").click(function () {
                var cardId = $(this).data("card-id");
                var categoryId = $(this).data("category-id");

                playMoveCallback(gameId, userId, cardId, categoryId);
            });

            //Now this has been called once, set the initial load as complete
            initialLoadComplete = true;
        }, 3000);
    }, initialLoadComplete ? 5000 : 1000);
}

function renderCard(gameCard) {
    //Variable to return the rendered card within
    var renderedHtml;

    //Make a AJAX call to render the HTML
    $.ajax({
        type: "POST",
        url: "/Game/RenderCard",
        async: false,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(gameCard),
        success: function (data) {
            renderedHtml = data;
        }
    });

    //Return the now rendered card
    return renderedHtml;
}

function showOpponentsCardToUser(opponentsCard, chosenCategoryId) {
    var renderedCard = renderCard(opponentsCard);

    //Set the opponent card to the rendered card and flip it over to see
    opponentPlayerCardBack.html(renderedCard);
    flipPlayerCard(opponentPlayerCardBack, true);

    //Get back the appropriate category selected
    if (chosenCategoryId != null) {
        var opponentPlayerCardCategory = getPlayerCardCategory(opponentPlayerCardBack, chosenCategoryId);
        var currentPlayerCardCategory = getPlayerCardCategory(currentPlayerCardBack, chosenCategoryId);

        //Add the class of selected to each category
        opponentPlayerCardCategory.addClass("selected");
        currentPlayerCardCategory.addClass("selected");

        //Remove the ability to select items from the categories
        opponentPlayerCardCategory.closest("table").removeClass("selectable");
        currentPlayerCardCategory.closest("table").removeClass("selectable");
    }
}

function flipPlayerCard(playerCardBack, reveal) {
    //If we want to reveal the card then add the class flipped to reveal, otherwise remove it to hide it
    if (reveal) {
        setGameStatus('&nbsp');
        playerCardBack.parent().addClass("flipped");
    }
    else {
        playerCardBack.parent().removeClass("flipped");
    }
}

function getPlayerCardCategory(cardBack, chosenCategoryId) {
    //Temporary variable to store the correct category
    var category;

    //Find the element card category matching the chosen categoryId
    cardBack.find(".card-category").each(function () {
        if ($(this).data("category-id") === chosenCategoryId) {
            category = $(this);
        }
    });

    //Return the result
    return category;
}

//Game conditions
function invalidGame() {
    //Show an alert to portray to the user they cannot join
    alert("You do not belong here");
}

function gameWon() {
    //Say the user won
    setWinStatus("You Won!");

    //Show the hud and animate
    $(".hud").fadeIn(1000, function () {
        //With stars
        $(".star").addClass("animate");
    });

    //Set the scores as appropriate
    setPlayerScores('~', '~');
}

function gameLost() {
    //Say the user lost
    setWinStatus("Hard luck <br> you lost.");

    //Show the hud and animate
    $(".hud").fadeIn(1000);

    //Set the scores as appropriate
    setPlayerScores('~', '~');
}

//Card win/loss/draw logic
function moveWon(gameCard, currentPlayerCardsCount, opposingPlayerCardsCount, playMoveCallback) {
    renderCurrentPlayerCard(gameCard, playMoveCallback);
    setPlayerScores(currentPlayerCardsCount, opposingPlayerCardsCount);
    setPlayableStatus(true);
    setGameStatus("You won the move! Well done!");
}

function moveLost(gameCard, currentPlayerCardsCount, opposingPlayerCardsCount, playMoveCallback) {
    renderCurrentPlayerCard(gameCard, playMoveCallback);
    setPlayerScores(currentPlayerCardsCount, opposingPlayerCardsCount);
    setPlayableStatus(false);
    setGameStatus("You lost the move! Better luck next time!");
}

function moveDrawn(gameCard, currentPlayerCardsCount, opposingPlayerCardsCount, isPlaying, playMoveCallback) {
    renderCurrentPlayerCard(gameCard, playMoveCallback);
    setPlayerScores(currentPlayerCardsCount, opposingPlayerCardsCount);
    setPlayableStatus(isPlaying);
    setGameStatus("It was a tie! No one loses :)");
}

//Players card deck
function processPlayersStack(currentPlayerCardsCount) {

    if (currentPlayerCardsCount > 2)
    {
        //Add both back
        $(".card.stack1").fadeIn(500);
        $(".card.stack2").fadeIn(500);
    }
    else if(currentPlayerCardsCount == 2)
    {
        //Only add 1
        $(".card.stack1").fadeIn(500);
        $(".card.stack2").fadeOut(500);
    }
    else 
    {
        //Show no cards
        $(".card.stack1").fadeOut();
        $(".card.stack2").fadeOut();
    }
}

//Various setters of information on the screen
function setPlayableStatus(play) {
    var peekContainer = $(".peek-container");

    //If this is the current plays move then allow them to play and change the UI
    //Otherwise lock the UI
    if (play) {
        //Show the peeks
        peekContainer.fadeIn(500);

        //Allow the player to play moves
        currentPlayerCardBack.addClass("playable");

        //Tell the player they are now playing
        setCurrentPersonPlaying("Your Move", play);
    }
    else {
        //Hide the peek container
        peekContainer.fadeOut(500);

        //Don't allow the player to play
        currentPlayerCardBack.removeClass("playable");
        currentPlayerCardBack.closest("table").removeClass("selectable");

        //Tell the player the opponent is playing
        setCurrentPersonPlaying("Opponents Move", play);
    }
}

function setGameStatus(status) {
    //Find the game status
    var gameStatus = $(".gameStatus");

    //Fade out the status then change it to the new one
    gameStatus.fadeOut(500, function () {
        gameStatus.html(status);
        gameStatus.fadeIn(500);
    });
}

function setWinStatus(text) {
    //Pre-pend the text as we don't want to remove the links
    $(".text").prepend(text);
}

function setCurrentPersonPlaying(personPlaying, playing) {
    //Get the container saying 'Your Move' or 'Opponents Move'
    var currentPersonPlaying = $(".move");

    //If the person is playing then set blue colours, otherwise yellow
    if (playing) {
        transitionColours(currentPersonPlaying, personPlaying, '#fce37d', '#edc740', '#333');
    }
    else {
        transitionColours(currentPersonPlaying, personPlaying, '#80A3D1', '#4679bd', '#fff');
    }
}

function transitionColours(element, text, lightColor, darkColor, textColour) {
    //Animate the background of an element
    element.animate({ backgroundColor: lightColor, color: textColour }, 500, function () {
        element.html(text);
        element.animate({ backgroundColor: darkColor }, 500);
    });
}

function setPlayerScores(currentPlayerCardsCount, opposingPlayerCardsCount) {
    //Get each respective card count element
    var opposingPlayerCardCountElement = opposingPlayerScoreCard.find(".card-count");
    var currentPlayerCardCountElement = currentPlayerScoreCard.find(".card-count");

    //Set the viewable cards
    processPlayersStack(currentPlayerCardsCount);

    //Fade out the previous score and fade in the new one
    opposingPlayerCardCountElement.fadeOut(500, function () {
        opposingPlayerCardCountElement.html(opposingPlayerCardsCount).fadeIn(500);
    });

    //Fade out the previous score and fade in the new one
    currentPlayerCardCountElement.fadeOut(500, function () {
        currentPlayerCardCountElement.html(currentPlayerCardsCount).fadeIn(500);
    });
}

function setOppositionsDetails(opposingPlayerName, currentPlayerCardCount, opposingPlayerCardCount) {
    //Set the oppositions username
    opposingPlayerScoreCard.find(".username").html(opposingPlayerName);

    //Setup the each players score
    setPlayerScores(currentPlayerCardCount, opposingPlayerCardCount);

    //Say that the game is ready to play
    setGameStatus("Opponent is ready, lets play!");

    setTimeout(function () {
        flipPlayerCard(currentPlayerCardBack, true);
    }, 5000);
}

//Disconnection logic
function playerDisconnected(callback) {
    //Set the game status as appropriate
    setGameStatus("Opponent has disconnected, attempting to re-connect...");
    disconnectedTimeout = setTimeout(function () {
        //As the player has been inactive for more than 'x' seconds, declare this user
        //the winner
        callback(gameId, userId);

        //Append the user won by disconnection
        setWinStatus("<br>Player disconnected");
    }, 15000);
}

function playerReconnected(opposingPlayerName) {
    //Clear the disconnected timeout as now the opponent has reconnected
    clearTimeout(disconnectedTimeout);

    //Set the status as appropriate
    setGameStatus("Opponent reconnected, continuing!");

    //Created a message in the chat say the player has reconnected
    appendMessage(opposingPlayerName, "Reconnected");
}

//Messaging Logic
function appendMessage(username, messageText) {
    //Find the messages container
    var messagesContainer = $(".messages");

    //Create a message
    var message = $("<div class='message'>");

    //Append the content
    message.html("<span>" + username + "</span>" + messageText);
    messagesContainer.append(message);

    //Ensure the messages scrolls to the bottom
    messagesContainer.scrollTop(messagesContainer.scrollTop() + 1000);

    //Animate the chat title so the user can see a message has arrived/sent
    $(".chat-title").animate({ backgroundColor: '#666' }, 500, function () {
        $(this).animate({ backgroundColor: '#333' }, 500);
    });
}