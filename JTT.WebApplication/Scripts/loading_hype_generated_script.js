//	HYPE.documents["loading"]

(function HYPE_DocumentLoader() {
	var resourcesFolderName = "loading.hyperesources";
	var documentName = "loading";
	var documentLoaderFilename = "loading_hype_generated_script.js";
	var mainContainerID = "loading_hype_container";

	// find the URL for this script's absolute path and set as the resourceFolderName
	try {
		var scripts = document.getElementsByTagName('script');
		for(var i = 0; i < scripts.length; i++) {
			var scriptSrc = scripts[i].src;
			if(scriptSrc != null && scriptSrc.indexOf(documentLoaderFilename) != -1) {
				resourcesFolderName = scriptSrc.substr(0, scriptSrc.lastIndexOf("/"));
				break;
			}
		}
	} catch(err) {	}

	// Legacy support
	if (typeof window.HYPE_DocumentsToLoad == "undefined") {
		window.HYPE_DocumentsToLoad = new Array();
	}
	
	// handle attempting to load multiple times
	if(HYPE.documents[documentName] != null) {
		var index = 1;
		var originalDocumentName = documentName;
		do {
			documentName = "" + originalDocumentName + "-" + (index++);
		} while(HYPE.documents[documentName] != null);
		
		var allDivs = document.getElementsByTagName("div");
		var foundEligibleContainer = false;
		for(var i = 0; i < allDivs.length; i++) {
			if(allDivs[i].id == mainContainerID && allDivs[i].getAttribute("HYPE_documentName") == null) {
				var index = 1;
				var originalMainContainerID = mainContainerID;
				do {
					mainContainerID = "" + originalMainContainerID + "-" + (index++);
				} while(document.getElementById(mainContainerID) != null);
				
				allDivs[i].id = mainContainerID;
				foundEligibleContainer = true;
				break;
			}
		}
		
		if(foundEligibleContainer == false) {
			return;
		}
	}
	
	var hypeDoc = new HYPE_160();
	
	var attributeTransformerMapping = {b:"i",c:"i",bC:"i",d:"i",aS:"i",M:"i",e:"f",aT:"i",N:"i",f:"d",O:"i",g:"c",aU:"i",P:"i",Q:"i",aV:"i",R:"c",bG:"f",aW:"f",aI:"i",S:"i",bH:"d",l:"d",aX:"i",T:"i",m:"c",bI:"f",aJ:"i",n:"c",aK:"i",bJ:"f",X:"i",aL:"i",A:"c",aZ:"i",Y:"bM",B:"c",bK:"f",bL:"f",C:"c",D:"c",t:"i",E:"i",G:"c",bA:"c",a:"i",bB:"i"};
	
	var resources = {"0":{n:"../../../Content/Images/Card-back.png",p:1}};
	
	var scenes = [{x:0,p:"600px",c:"#FFFFFF",onSceneTimelineCompleteActions:[{type:1,transition:1,sceneSymbol:1}],v:{"3":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"2",d:215},"4":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"1",d:215},"5":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"3",d:215}},n:"Start",T:{kTimelineDefaultIdentifier:{d:1.25,i:"kTimelineDefaultIdentifier",n:"Main Timeline",a:[{f:"2",t:0,d:1,i:"a",e:16,s:227,o:"5"},{f:"2",t:0.16,d:1.09,i:"a",e:438,s:227,o:"3"}],f:30}},o:"1"},{x:1,p:"600px",c:"#FFFFFF",onSceneTimelineCompleteActions:[{timelineIdentifier:"kTimelineDefaultIdentifier",type:3}],v:{"8":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"1",d:215},"12":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"4",d:215},"9":{o:"content-box",h:"0",x:"visible",a:16,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"3",d:215},"7":{o:"content-box",h:"0",x:"visible",a:438,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"2",d:215},"11":{o:"content-box",h:"0",x:"visible",a:227,q:"100% 100%",b:12,j:"absolute",r:"inline",c:146,k:"div",z:"5",d:215}},n:"Loop",T:{kTimelineDefaultIdentifier:{d:1.26,i:"kTimelineDefaultIdentifier",n:"Main Timeline",a:[{f:"2",t:0,d:1.26,i:"b",e:12,s:12,o:"12"},{f:"2",t:0,d:1.05,i:"a",e:16,s:227,o:"11"},{f:"2",t:1,d:0.26,i:"a",e:438,s:227,o:"12"}],f:30}},o:"10"}];
	
	var javascripts = [];
	
	var functions = {};
	var javascriptMapping = {};
	for(var i = 0; i < javascripts.length; i++) {
		try {
			javascriptMapping[javascripts[i].identifier] = javascripts[i].name;
			eval("functions." + javascripts[i].name + " = " + javascripts[i].source);
		} catch (e) {
			hypeDoc.log(e);
			functions[javascripts[i].name] = (function () {});
		}
	}
	
	hypeDoc.setAttributeTransformerMapping(attributeTransformerMapping);
	hypeDoc.setResources(resources);
	hypeDoc.setScenes(scenes);
	hypeDoc.setJavascriptMapping(javascriptMapping);
	hypeDoc.functions = functions;
	hypeDoc.setCurrentSceneIndex(0);
	hypeDoc.setMainContentContainerID(mainContainerID);
	hypeDoc.setResourcesFolderName(resourcesFolderName);
	hypeDoc.setShowHypeBuiltWatermark(0);
	hypeDoc.setShowLoadingPage(false);
	hypeDoc.setDrawSceneBackgrounds(false);
	hypeDoc.setGraphicsAcceleration(true);
	hypeDoc.setDocumentName(documentName);

	HYPE.documents[documentName] = hypeDoc.API;
	document.getElementById(mainContainerID).setAttribute("HYPE_documentName", documentName);

	hypeDoc.documentLoad(this.body);
}());

