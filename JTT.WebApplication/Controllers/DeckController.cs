﻿using JTT.Domain.DTO;
using JTT.Interfaces;
using JTT.WebApplication.Filters;
using JTT.WebApplication.Models;
using JTT.WebApplication.Models.Game;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTT.WebApplication.Utilities;

using JTT.SharedComponents;

namespace JTT.WebApplication.Controllers
{
    public class DeckController : Controller
    {
        private readonly IDeckBusinessLogic _deckBusinessLogic;
        private readonly IDeckCategoryBusinessLogic _deckCategoryBusinessLogic;
        private readonly ICardBusinessLogic _cardBusinessLogic;
        private readonly ICardCategoryBusinessLogic _cardCategoryBusinessLogic;

        CloudStorageAccount storageAccount;
        CloudBlobClient blobClient;
        CloudBlobContainer container;

        public DeckController(IDeckBusinessLogic deckBusinessLogic, IDeckCategoryBusinessLogic deckCategoryBusinessLogic, ICardBusinessLogic cardBusinessLogic, ICardCategoryBusinessLogic cardCategoryBusinessLogic)
        {
            _deckBusinessLogic = deckBusinessLogic;
            _deckCategoryBusinessLogic = deckCategoryBusinessLogic;
            _cardBusinessLogic = cardBusinessLogic;
            _cardCategoryBusinessLogic = cardCategoryBusinessLogic;
        }

        [TopTrumpsAuthorise]
        public ActionResult Index()
        {
            var decks = _deckBusinessLogic.GetAllDecks();
            return View(decks);
        }

        public new ActionResult View(string id)
        {
            Guid deckId = Guid.Parse(id);

            Deck deck = _deckBusinessLogic.GetById(deckId);
            List<DeckCategory> deckCategories = _deckCategoryBusinessLogic.GetDeckCategoriesForDeck(deckId);
            List<Card> cards = _cardBusinessLogic.GetCardsForDeck(deck.Id);

            TopTrumpsDeck ttDeck = new TopTrumpsDeck(deck.Id, deck.Name, new List<TopTrumpsCard>());

            foreach (Card card in cards)
            {
                TopTrumpsCard ttCard = new TopTrumpsCard(card.Id, card.Name, new List<TopTrumpsCategory>(), BlobUtilities.GetBlobAddress(deck.Name, card.Id));

                foreach (CardCategory category in card.CardCategories)
                {
                    TopTrumpsCategory ttCategory = new TopTrumpsCategory()
                    {
                        CategoryId = category.Id,
                        Name = deckCategories.Find(c => c.Id == category.DeckCategoryId).Name,
                        Value = category.Score
                    };

                    ttCard.TopTrumpsCategories.Add(ttCategory);
                }

                ttDeck.TopTrumpsCards.Add(ttCard);
            }

            return View(ttDeck);
        }

        public ActionResult All()
        {
            List<Deck> decks = _deckBusinessLogic.GetAllDecks();
            List<TopTrumpsDeck> ttdecks = new List<TopTrumpsDeck>();

            foreach(Deck deck in decks)
            {
                Card card = _cardBusinessLogic.GetFirstCardForDeck(deck.Id);
                
                List<TopTrumpsCard> cards = new List<TopTrumpsCard>{ 
                    new TopTrumpsCard(card.Id, card.Name, null, BlobUtilities.GetBlobAddress(deck.Name, card.Id) ) 
                };

                ttdecks.Add(
                    new TopTrumpsDeck(
                        deck.Id, 
                        deck.Name,
                        cards
                        )
                );
            }

            return View(ttdecks);
        }

        public ActionResult Create()
        {
            DeckModel model = new DeckModel
            {
                DeckName = "Joel Deck",
                DeckCategories = new List<DeckCategoryModel>() 
                {  
                    new DeckCategoryModel(){ CategoryName = string.Empty, Index=0 },
                    new DeckCategoryModel(){ CategoryName = string.Empty, Index=1 },
                    new DeckCategoryModel(){ CategoryName = string.Empty, Index=2 },
                    new DeckCategoryModel(){ CategoryName = string.Empty, Index=3 }

                    //Default categories for testing purposes
                    //new DeckCategoryModel(){ CategoryName = "Intelligence", Index=0 },
                    //new DeckCategoryModel(){ CategoryName = "Cuteness", Index=1 },
                    //new DeckCategoryModel(){ CategoryName = "Emotional", Index=2 },
                    //new DeckCategoryModel(){ CategoryName = "Boyfriend Material", Index=3 },
                    //new DeckCategoryModel(){ CategoryName = "Dress Sense", Index=4 }
                }
            };
            return View(model);
        }

        public ActionResult CreateDeck(string deckName, ICollection<DeckCategoryModel> deckCategories)
        {
            DeckModel model = new DeckModel
            {
                DeckName = deckName,
                DeckCategories = deckCategories
            };

            return View(model);
        }

        public PartialViewResult AddDeckCategory(int index)
        {
            return PartialView("PartialViews/_DeckCategoryPartial", new DeckCategoryModel() { CategoryName = string.Empty, Index = index });
        }

        public ActionResult SaveDeck(string deckName, ICollection<DeckCategoryModel> deckCategories, ICollection<CardModel> cards, IEnumerable<HttpPostedFileBase> cardImages)
        {
            SetupStorageContainer(deckName);

            //Save the Deck
            Guid deckId = _deckBusinessLogic.CreateDeck(new Deck() { Name = deckName });

            ////Save the Deck Categories
            foreach (DeckCategoryModel deckCategory in deckCategories)
            {
                Guid dcId = _deckCategoryBusinessLogic.CreateDeckCategory(new DeckCategory() { Id = Guid.NewGuid(), DeckId = deckId, Name = deckCategory.CategoryName });

                //Keep track of the deck Category GUIDs so that Card Categories can be linked up
                foreach (CardModel card in cards)
                {
                    foreach (CardCategoryModel cardCategory in card.CardCategories)
                    {
                        if (cardCategory.Index == deckCategory.Index)
                        {
                            cardCategory.DeckCategoryId = dcId;
                        }
                    }
                }
            }


            //Save the Cards
            int imageInc = 0;
            foreach (CardModel card in cards)
            {
                //Get the card image from the list of files
                HttpPostedFileBase image = cardImages.ElementAt(imageInc);
                imageInc++;

                //Create a guid for the card
                Guid cardGuid = Guid.NewGuid();

                // Retrieve reference to a blob named with this guid
                CloudBlockBlob blockBlob = container.GetBlockBlobReference( cardGuid.ToString() );

                //Upload the blob
                blockBlob.UploadFromStream(image.InputStream);

                //Create Card to Save
                Card newCard = new Card
                {
                    Id = cardGuid,
                    Name = card.CardName,
                    DeckId = deckId
                };

                //Save the card object
                _cardBusinessLogic.CreateCard(newCard);

                List<CardCategory> cardCategories = new List<CardCategory>();

                //Save the Card Category Values
                foreach (CardCategoryModel cardCategory in card.CardCategories)
                {
                    cardCategories.Add(new CardCategory
                    { 
                        Id = Guid.NewGuid(), 
                        DeckCategoryId = cardCategory.DeckCategoryId, 
                        Score = cardCategory.Value, 
                        CardId = newCard.Id 
                    });
                }

                //Save the Card Category objects
                _cardCategoryBusinessLogic.CreateCardCategory(cardCategories);
            }

            return RedirectToAction("View", new { id = deckId.ToString() });
        }

        public void SetupStorageContainer(string deckname)
        {
            //Retrieve the connection string
            string connStr = ConfigurationUtilities.GetConfigurationSetting("AzureConnectionString");

            //Retrieve storage account from connection string.
            storageAccount = CloudStorageAccount.Parse(connStr);

            //Create the blob client.
            blobClient = storageAccount.CreateCloudBlobClient();

            //Retrieve a reference to a container. 
            container = blobClient.GetContainerReference(deckname.ToLower().Replace(" ", string.Empty));

            //Create the container if it doesn't already exist.
            container.CreateIfNotExists();

            //Make the card images publicly accessible
            container.SetPermissions(
            new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
        }
    }
}