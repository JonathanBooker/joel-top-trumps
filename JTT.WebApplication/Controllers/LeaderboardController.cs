﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clockwork;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.WebApplication.Models;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.Controllers
{
    public class LeaderboardController : Controller
    {
        private readonly IGameBusinessLogic _gameBusinessLogic;
        private readonly IUserBusinessLogic _userBusinessLogic;
        private readonly IDeckBusinessLogic _deckBusinessLogic;

        public LeaderboardController(IGameBusinessLogic gameBusinessLogic, IUserBusinessLogic userBusinessLogic,
            IDeckBusinessLogic deckBusinessLogic)
        {
            _gameBusinessLogic = gameBusinessLogic;
            _userBusinessLogic = userBusinessLogic;
            _deckBusinessLogic = deckBusinessLogic;
        }

        public ActionResult Index()
        {
            var leaderboardPlayers = _gameBusinessLogic.GetRankedUsers();
            var decks = _deckBusinessLogic.GetAllDecks();

            return View(new LeaderboardModel
            {
                LeaderboardPlayers = leaderboardPlayers,
                Decks = decks
            });
        }

        [HttpPost]
        public string Challenge(Guid deckId, Guid userId)
        {
            var user = _userBusinessLogic.GetUserById(userId);
            var loggedInUser = _userBusinessLogic.GetUserById(TopTrumpsProvider.GetLoggedInUserId());
            var deck = _deckBusinessLogic.GetById(deckId);

            if (user != null && deck != null)
            {
                try
                {
                    var action = Url.Action("Play", "Game", new { id = Guid.NewGuid(), deckId });
                    var api = new API(ConfigurationUtilities.GetConfigurationSetting("ClockworkApiKey"));
                    var bitlyLink = BitlyUtilities.GetBitlyLink(action);

                    var result = api.Send(new SMS
                    {
                        To = user.PhoneNumber,
                        Message = "Hey " + user.Forename + ", " + loggedInUser.Username + " has challenged you!\n" + bitlyLink
                    });

                    return result.Success ? bitlyLink : string.Empty;
                }
                catch (Exception exception)
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }

        [HttpPost]
        public ActionResult SearchUsers(string searchText)
        {
            var users = _gameBusinessLogic.GetSearchUsers(searchText);
            var decks = _deckBusinessLogic.GetAllDecks();

            return PartialView("_SearchableLeaderboard", 
                new LeaderboardModel
                {
                    LeaderboardPlayers = users,
                    Decks = decks
                });
        }
    }
}