﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTT.Domain.Enums;
using JTT.Interfaces;
using JTT.WebApplication.Models;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILoginBusinessLogic _loginBusinessLogic;

        public LoginController(ILoginBusinessLogic loginBusinessLogic)
        {
            _loginBusinessLogic = loginBusinessLogic;
        }

        public ActionResult Index(string returnUrl)
        {
            return View(new LoginModel { RedirectURL = returnUrl });
        }

        public ActionResult Login(LoginModel loginModel)
        {
            var topTrumpsProvider = new TopTrumpsProvider(_loginBusinessLogic);
            var result = topTrumpsProvider.LoginUser(loginModel.Username, loginModel.Password);

            if (result == LoginResult.Success)
            {
                if(loginModel.RedirectURL != null)
                {
                    return Redirect(loginModel.RedirectURL);
                }

                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index");
        }

        public ActionResult LoginAsAnonymous()
        {
            TopTrumpsProvider.LoginAsAnonymous();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            TopTrumpsProvider.LogoutUser();
            return RedirectToAction("Index");
        }
    }
}