﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using JTT.WebApplication.Filters;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.Controllers
{

    //Basically a quick way of securing a controller
    [TopTrumpsAuthorise]
    public class AuthorisedController : Controller
    {
        public Guid CurrentUserId
        {
            get
            {
                return TopTrumpsProvider.GetLoggedinUser().UserId;
            }
        }
    }
}
