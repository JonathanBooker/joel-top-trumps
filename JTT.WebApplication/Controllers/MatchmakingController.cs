﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTT.WebApplication.Models.Matchmaking;

namespace JTT.WebApplication.Controllers
{
    public class MatchmakingController : AuthorisedController
    {
        [HttpGet]
        public ActionResult SeekMatch(Guid? id = null)
        {
            return View(new SeekMatchModel
            {
                DeckId = id
            });
        }
    }
}