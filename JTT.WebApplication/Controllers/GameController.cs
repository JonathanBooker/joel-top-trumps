﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JTT.Interfaces;
using JTT.WebApplication.Models;
using JTT.WebApplication.Models.Game;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.Controllers
{
    public class GameController : AuthorisedController
    {
        private readonly IDeckBusinessLogic _deckBusinessLogic;

        public GameController(IDeckBusinessLogic deckBusinessLogic)
        {
            _deckBusinessLogic = deckBusinessLogic;
        }

        public ActionResult Play(Guid id, Guid deckId)
        {
            var deck = _deckBusinessLogic.GetById(deckId);

            if (deck != null)
            {
                return View(new GameModel
                {
                    GameId = id,
                    DeckId = deckId,
                    UserId = CurrentUserId,
                    DeckName = deck.Name
                });
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PrivateGame(Guid deckId)
        {
            var action = Url.Action("Play", "Game", new { id = Guid.NewGuid(), deckId });
            return View(new PrivateGameModel
            {
                BitlyUrl = BitlyUtilities.GetBitlyLink(action)
            });
        }

        [HttpPost]
        public ActionResult RenderCard(TopTrumpsCard topTrumpsCard)
        {
            return PartialView("_Card", topTrumpsCard);
        }
    }
}