﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.Filters
{
    public class TopTrumpsAuthoriseAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = TopTrumpsProvider.GetLoggedinUser();
            return user != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(
                    new
                    {
                        controller = "Login",
                        action = "Index",
                        returnUrl = filterContext.RequestContext.HttpContext.Request.RawUrl
                    })
            );
        }
    }
}
