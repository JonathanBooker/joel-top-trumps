﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.WebApplication.Models.Matchmaking;
using Microsoft.AspNet.SignalR;

namespace JTT.WebApplication.SignalR
{
    public class MatchmakingHub : Hub
    {
        //A list containing the players awaiting to be matched up
        private static List<AwaitingPlayer> _awaitingPlayers = new List<AwaitingPlayer>();

        private readonly IDeckBusinessLogic _deckBusinessLogic;

        public MatchmakingHub(IDeckBusinessLogic deckBusinessLogic)
        {
            _deckBusinessLogic = deckBusinessLogic;
        }

        public override Task OnConnected()
        {
            //Get the players who have been waiting longer then 30 seconds
            var expiredMatchmakingPlayers = _awaitingPlayers.Where(p => 
                DateTime.Parse(DateTime.Now.ToString()).Subtract(p.TimeAdded).TotalSeconds > 30).ToList();

            //For every player waiting longer then this time, remove them
            foreach (var expiredMatchmakingPlayer in expiredMatchmakingPlayers)
            {
                //Get rid of the player from the groups list
                Groups.Remove(expiredMatchmakingPlayer.ConnectionId, expiredMatchmakingPlayer.UserId.ToString());

                //Remove the player from waiting
                _awaitingPlayers.Remove(expiredMatchmakingPlayer);
            }

            //Continue as normal
            return base.OnConnected();
        }

        public void EstablishWaitingPlayer(Guid userId, Guid? deckId)
        {
            //First a check to see if the player isn't just refreshing the page so it doesn't match himself to himself
            var waitingPlayer = _awaitingPlayers.SingleOrDefault(p => p.UserId == userId);
            if (waitingPlayer != null)
            {
                waitingPlayer.TimeAdded = DateTime.Now;
                return;
            }

            //Make a copy of the waiting players
            var playersWaiting = new List<AwaitingPlayer>(_awaitingPlayers);

            //If the player is wanting to play a certain deck then only allow them to be paired with players who want to play that deck
            if (deckId.HasValue)
            {
                playersWaiting = playersWaiting.Where(p => p.DeckId.HasValue && p.DeckId.Value == deckId.Value).ToList();
            }

            //If there are any awaiting players then we will match make the current connecting user to them
            if (playersWaiting.Any())
            {
                //Get the player who has been awaiting the longest
                var longestWaitingPlayer = playersWaiting.OrderByDescending(p => p.TimeAdded).FirstOrDefault();

                //Ensure they aren't null
                if (longestWaitingPlayer != null)
                {
                    //Remove the original longest waiting player
                    _awaitingPlayers.Remove(longestWaitingPlayer);

                    //Get a new game identifier to use for the game being created
                    var newGameId = Guid.NewGuid();

                    //If both players are not picky about the deck chosen then a random one will be selected for them
                    if (!longestWaitingPlayer.DeckId.HasValue || !deckId.HasValue)
                    {
                        //Get all the decks
                        var decks = _deckBusinessLogic.GetAllDecks();

                        //Randomise the items in the list
                        decks.Shuffle();

                        //Assign the deck to the first
                        deckId = decks.First().Id;
                    }

                    //Notify the first player about the game
                    Clients.Client(longestWaitingPlayer.ConnectionId).startMatch(newGameId, deckId);

                    //Asynchronously start the other player a little later to stop hub connection issues
                    Task.Factory.StartNew(() =>
                    {
                        //Notify the first player
                        Thread.Sleep(2000);
                        Clients.Client(Context.ConnectionId).startMatch(newGameId, deckId);
                    });
                }
            }
            else
            {
                //If there is no one to be match made then add them to a waiting list
                _awaitingPlayers.Add(new AwaitingPlayer
                {
                    UserId = userId,
                    TimeAdded = DateTime.Now,
                    ConnectionId = Context.ConnectionId,
                    DeckId = deckId
                });
            }
        }
    }
}
