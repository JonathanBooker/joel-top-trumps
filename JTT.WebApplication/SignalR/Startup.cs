﻿using JTT.SharedComponents;
using JTT.WebApplication.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace JTT.WebApplication.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            /*
             * Points to cover in the report:
             *  - Talk about how the costs may scale over time
             *      => Although Azure should auto-scale you can talk about how you could scale it quickly to manage demand
             *      => Talk about manually expanding VM machines of the web roles
             *      => Creating more web role instances within the application web role
             *  - How does the Azure platform better others?
             * 
             * There are generally two ways in which to scale out a web application, these being:
             *  - Scale up  = More RAM, hard drive space, etc
             *  - Scale out = More Servers
             *  
             * Keeping inline with the concept of cloud based services and there 'scale out' nature, we will be 
             * utilising these features of SignalR. For this to be possible it is necessary to use a 'backplane' which
             * will be able to sync the messages being sent between clients across SignalR.
             * A backplane is a single point at which messages all go through and handled. The idea is that rather then
             * the message going straight to the client, which would be suitable on a single server, the message goes to 
             * the backplane first. It is within the backplane that the message is replicated and then distributed to
             * all nodes/VM's subscribed to it. This is because we do not know which server the client we are sending messages
             * to is so each node/VM needs to know about this message. Once the VM knows about the message it will send the
             * message out as appropriate via SignalR
             * 
             * We could use three alternative methods in order to do this depending on the PaaS system used. 
             * 1) Azure Service Bus
             * 2) SQL Server Database
             * 3) Reddis
             */

            /*
             * AZURE
             *  - With this application being C# based and going to be based in the cloud it would make the most sense to host the
             *    backplane within Azure
             *  - The benefit of this is that the message bus is scaled as appropriate as messages are created
             *  - The downside is that there is a limit on the number of messages allowed within a timeframe before prices go up
             *      => 1,000,000 per month for $1
             */
            //Use Azure Service Bus to handle the client connections across multiple servers/VM's, etc
            var serviceBusConnectionString = ConfigurationUtilities.GetConnectionString("AzureServiceBusConnectionString");
            GlobalHost.DependencyResolver.UseServiceBus(serviceBusConnectionString, "TopTrumpsGame");  


            /*
             * SQL Server
             *  - It is possible to use a SQL Database instance, either dedicated or within the application itself to handle
             *    the messages being sent
             *  - This is useful as it means that you don't have to run applications through Azure service which can be expensive
             *  - The downside is that it may not be as fast as it is done transactionally
             *  - To use this it simply requires the necessary NuGet packages and a database that is enabled as a 'Broker' database
             */
            //Use an MS SQL database to handle the client connections across multiple servers/VM's, etc
            //var sqlServerConnectionString = ConfigurationUtilities.GetConnectionString("JoelTopTrumpsDatabaseSignalRContainer");
            //GlobalHost.DependencyResolver.UseSqlServer(sqlServerConnectionString);

            /*
             * REDDIS
             *  - This is an Open Source message bus system similar to that of Azure's service bus
             *  - This runs in the same manner in that it acts as a means in which to sync messages across VM's or nodes
             *  - This in theory is a good option as it is optimised for fast inserts, reads etc due to its NoSQL nature
             *  - Supported by Microsoft: https://github.com/MSOpenTech/Redis
             *  - Disadvantage is that this is extra setup for another service that is a lot more complicated then the other options
             */
            //Use a Reddis connection in order to handle the client connections across multiple servers
            //GlobalHost.DependencyResolver.UseRedis("reddis.simple-storage.co.uk", 6379, "Pa55w0rd", "TopTrumps");  

            //Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}