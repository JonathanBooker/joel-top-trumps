﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using JTT.Domain.Core;
using JTT.Domain.DTO;
using JTT.Interfaces;
using JTT.SharedComponents;
using JTT.WebApplication.Models.Game;
using Microsoft.AspNet.SignalR;
using JTT.WebApplication.Utilities;

namespace JTT.WebApplication.SignalR
{
    public class TopTrumpsHub : Hub
    {
        //A means in which to randomly assign cards
        private static readonly Random _systemRandom = new Random();

        //A list that allows us to keep track of the games that are in progress
        private static List<TopTrumpsGame> _topTrumpsGames = new List<TopTrumpsGame>();

        //Allows access to decks stored within the database
        private readonly IDeckBusinessLogic _deckBusinessLogic;
        private readonly IUserBusinessLogic _userBusinessLogic;
        private readonly IGameBusinessLogic _gameBusinessLogic;

        public TopTrumpsHub(IDeckBusinessLogic deckBusinessLogic, IUserBusinessLogic userBusinessLogic, IGameBusinessLogic gameBusinessLogic)
        {
            _deckBusinessLogic = deckBusinessLogic;
            _userBusinessLogic = userBusinessLogic;
            _gameBusinessLogic = gameBusinessLogic;
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            //Get the ConnectionId of the user who just disconnected
            var disconnectedUserConnectionId = Context.ConnectionId;

            //Get the associated game with that user
            var associatedGame = _topTrumpsGames.SingleOrDefault(g => g.TopTrumpsPlayers.Any(p => p.ConnectionId ==
                disconnectedUserConnectionId));

            //If there is a game then proceed to notify the other player in the game the other player has 
            //disconnected
            if (associatedGame != null)
            {
                //Find the player who is connected
                var disconnectedUser = associatedGame.TopTrumpsPlayers.SingleOrDefault(p => p.ConnectionId == disconnectedUserConnectionId);

                //Find the player who is connected
                var connectedUser = associatedGame.TopTrumpsPlayers.SingleOrDefault(p => p.ConnectionId != disconnectedUserConnectionId);

                //If there is one
                if (connectedUser != null && disconnectedUser != null)
                {
                    //Set the disconnected player as inactive
                    disconnectedUser.PlayerInactive = true;

                    //Notify them the opposing player has disconnected
                    Clients.Client(connectedUser.ConnectionId).playerDisconnected();
                }
            }

            //Standard disconnected behaviour
            return base.OnDisconnected(stopCalled);
        }

        public void EstablishConnection(Guid gameId, Guid deckId, Guid userId)
        {
            //If the game is already ongoing then set the current user on his way with the game
            if (_topTrumpsGames.Any())
            {
                var currentOngoingGame = _topTrumpsGames.FirstOrDefault(g => g.GameId == gameId);
                if (currentOngoingGame != null && currentOngoingGame.TopTrumpsPlayers.Count == 2)
                {
                    StartGameForUser(currentOngoingGame, userId);
                }
            }

            //If the game doesn't exist then create one, else if the game is going and has 1 player in it
            if (_topTrumpsGames.All(g => g.GameId != gameId))
            {
                ////Get back the deck
                var deck = _deckBusinessLogic.GetById(deckId);

                var topTrumpCards = deck.Cards.Select(c => new TopTrumpsCard
                {
                    CardId = c.Id,
                    Name = c.Name,
                    TopTrumpsCategories = new List<TopTrumpsCategory>(c.CardCategories.Select(cc => 
                        new TopTrumpsCategory(cc.DeckCategoryId, cc.DeckCategoryName, cc.Score))),
                    ImageAddress = BlobUtilities.GetBlobAddress(deck.Name, c.Id)
                }).ToList();

                //Initialise up a new game and assign the cards that are being used to the available cards
                var newGame = new TopTrumpsGame(gameId, topTrumpCards);
                
                //Sort out the new player of the game
                newGame = CreatePlayerAndAssignCards(userId, newGame);

                //Add the new game to the currently on going games
                _topTrumpsGames.Add(newGame);

                //Start the game!
                StartGameForUser(newGame, userId);
            }
            else
            {
                //Get the current ongoing game
                var currentOngoingGame = _topTrumpsGames.Single(g => g.GameId == gameId);

                //Only if the game hasn't already got 2 players allow the addition of another
                if (currentOngoingGame.TopTrumpsPlayers.Count < 2)
                {
                    //Assign the new player to the game
                    currentOngoingGame = CreatePlayerAndAssignCards(userId, currentOngoingGame);

                    //Start the game!
                    StartGameForUser(currentOngoingGame, userId);
                }
            }
        }

        public void PlayMove(Guid gameId, Guid userId, Guid cardId, Guid selectedCategoryId)
        {
            //Get the current game that is going on
            var currentGame = GetTopTrumpsGame(gameId);

            //If the game exists then we can continue
            //Else the client trying to connect needs to be informed there is no game for the ID sent through
            if (currentGame != null)
            {
                //Set who played the last move
                currentGame.UserIdOfLastPlayed = userId;

                //Get the current player that is making a move
                var currentPlayer = GetGamePlayer(currentGame, userId);

                //Get the card they are currently playing with
                var currentPlayerCard = currentPlayer.TopTrumpsCards.SingleOrDefault(c => c.CardId == cardId);

                //This should not happen but an extra precaution that the card the user is playing with exists
                if (currentPlayerCard != null)
                {
                    //For now a dangerous assumption about there being max 2 players as we get the player that isn't matching the userId sent through
                    var opposingPlayer = currentGame.TopTrumpsPlayers.FirstOrDefault(p => p.UserId != userId);

                    //Ensure that there is a second player in the game
                    if (opposingPlayer != null)
                    {
                        //Get the card that the opposing player should be played with at this moment in time
                        var opposingPlayersCard = opposingPlayer.TopTrumpsCards.First();

                        //Send the card over to the opponent so they can see what was played
                        Clients.Client(currentPlayer.ConnectionId).showOpponentCardPlayed(opposingPlayersCard, selectedCategoryId);
                        Clients.Client(opposingPlayer.ConnectionId).showOpponentCardPlayed(currentPlayerCard, selectedCategoryId);

                        //Get the card categories being currently being played for each card
                        var currentPlayerCardCategory = currentPlayerCard.TopTrumpsCategories.Single(c => c.CategoryId == selectedCategoryId);
                        var opposingPlayerCardCategory = opposingPlayersCard.TopTrumpsCategories.Single(c => c.CategoryId == selectedCategoryId);

                        //If there is a draw then shuffle the players cards and play again
                        //Else there has to be a winner so determine them
                        if (currentPlayerCardCategory.Value == opposingPlayerCardCategory.Value)
                        {
                            //Carry out draw action
                            MoveDraw(currentPlayer, opposingPlayer);
                        }
                        else
                        {
                            //If the category demands the value be the lowest that wins then play those rules
                            //Else play highest wins
                            if (currentPlayerCardCategory.LowestWins)
                            {
                                //If the current players card value is lower then their opponents then they won
                                //Else if the vice versa is the case then the opposing player won
                                if (currentPlayerCardCategory.Value < opposingPlayerCardCategory.Value)
                                {
                                    //Handle the current player winning
                                    HandlePlayerMove(currentGame, currentPlayer, opposingPlayer, currentPlayerCard, opposingPlayersCard);
                                }
                                else if (opposingPlayerCardCategory.Value < currentPlayerCardCategory.Value)
                                {
                                    //Handle the opposing player winning
                                    HandlePlayerMove(currentGame, opposingPlayer, currentPlayer, opposingPlayersCard, currentPlayerCard);
                                }
                            }
                            else
                            {
                                //If the current players card value is higher then their opponents then they won
                                //Else if the vice versa is the case then the opposing player won
                                if (currentPlayerCardCategory.Value > opposingPlayerCardCategory.Value)
                                {
                                    //Handle the current player winning
                                    HandlePlayerMove(currentGame, currentPlayer, opposingPlayer, currentPlayerCard, opposingPlayersCard);
                                }
                                else if (opposingPlayerCardCategory.Value > currentPlayerCardCategory.Value)
                                {
                                    //Handle the opposing player winning
                                    HandlePlayerMove(currentGame, opposingPlayer, currentPlayer, opposingPlayersCard, currentPlayerCard);
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Send a message to the connecting client that the game they are trying to play a move on no longer exists
                    Clients.Client(Context.ConnectionId).noGameExists();
                }
            }
        }

        public void SendMessage(Guid gameId, Guid senderId, string message)
        {
            //Get the current game that is going on
            var currentGame = GetTopTrumpsGame(gameId);

            //Get the player who has sent the message
            var sendingPlayer = GetGamePlayer(currentGame, senderId);

            //For every player in the game send them a message
            foreach (var topTrumpsPlayer in currentGame.TopTrumpsPlayers)
            {
                //Notify everyone listening to the event
                Clients.Client(topTrumpsPlayer.ConnectionId).appendMessage(sendingPlayer.GetMessagePlayerName(), message);
            }
        }

        private void HandlePlayerMove(TopTrumpsGame topTrumpsGame, TopTrumpsPlayer winningPlayer, TopTrumpsPlayer losingPlayer, TopTrumpsCard winningPlayersCard, 
            TopTrumpsCard losingPlayerCard)
        {
            //Put the card the player just used to the back of the deck by removing and adding it
            winningPlayer.TopTrumpsCards.Remove(winningPlayersCard);
            winningPlayer.TopTrumpsCards.Add(winningPlayersCard);

            //The current player card is better
            //Swap the card over to the winning player
            winningPlayer.TopTrumpsCards.Add(losingPlayerCard);
            losingPlayer.TopTrumpsCards.Remove(losingPlayerCard);

            //Get the cards that each player will be playing next after this move
            //The winning player is guaranteed to have another card and the first would be the new one just added
            //For the losing player this needs to be checked
            var winningPlayerNextCard = winningPlayer.TopTrumpsCards.Skip(1).First();
            var losingPlayerNextCard = losingPlayer.TopTrumpsCards.FirstOrDefault();

            //If the opposing player has a card to play
            //Else the game is over and so notify the players of the winner
            if (losingPlayerNextCard != null)
            {
                //Notify each player of what has taken place, notify the current player they are still playing and the losing player they are not playing next turn
                Clients.Client(losingPlayer.ConnectionId).moveLost(losingPlayerNextCard, losingPlayer.TopTrumpsCards.Count, winningPlayer.TopTrumpsCards.Count);

                //Tell the winning player last that they have won the move 
                Task.Factory.StartNew(() =>
                {
                    //Wait 1.5 seconds before telling the winning player
                    Thread.Sleep(1500);
                    Clients.Client(winningPlayer.ConnectionId).moveWon(winningPlayerNextCard, winningPlayer.TopTrumpsCards.Count, losingPlayer.TopTrumpsCards.Count);
                });
            }
            else
            {
                //Carry out won behaviour
                GameWon(topTrumpsGame, winningPlayer, losingPlayer);
            }
        }

        public void PeekOpponentsCard(Guid gameId, Guid currentPlayerId)
        {
            //Get the current game that is going on
            var currentGame = GetTopTrumpsGame(gameId);

            //If the game exists then we can continue
            //Else the client trying to connect needs to be informed there is no game for the ID sent through
            if (currentGame != null)
            {
                //Get the opposing player
                var currentPlayer = GetGamePlayer(currentGame, currentPlayerId);
                var opposingPlayer = GetGameOpposingPlayer(currentGame, currentPlayerId);

                //So long as there is a opposing player
                if (currentPlayer != null && opposingPlayer != null)
                {
                    //So long as the current player has peeks left
                    //Else tell the current player they cannot peek
                    if (currentPlayer.DoesPlayerHavePeeks())
                    {
                        //Get the opposing players card they are using
                        var opposingPlayerCard = opposingPlayer.TopTrumpsCards.First();

                        //Remove a player peek
                        currentPlayer.DecrementPlayerPeeks();

                        //Send the opponents card to peek at the the calling player
                        Clients.Caller.peekOpponentsCard(opposingPlayerCard);

                        //Tell the opponent they have been peeked on
                        Clients.Client(opposingPlayer.ConnectionId).opponentPeekedCard();
                    }
                    else
                    {
                        Clients.Caller.noMorePeeks();
                    }
                }
            }
        }

        public void GameWonByDisconnect(Guid gameId, Guid userId)
        {
            //Get the current game
            var currentGame = GetTopTrumpsGame(gameId);

            //If there is a game being played
            if (currentGame != null)
            {
                var currentPlayer = GetGamePlayer(currentGame, userId);
                var opposingPlayer = GetGameOpposingPlayer(currentGame, userId);

                if (currentPlayer != null && opposingPlayer != null && opposingPlayer.PlayerInactive)
                {
                    //Set the game as won for the still connected player
                    GameWon(currentGame, currentPlayer, opposingPlayer);
                }
            }
        }

        private void MoveDraw(TopTrumpsPlayer currentPlayer, TopTrumpsPlayer opposingPlayer)
        {
            //Both players drew so reshuffle their cards
            currentPlayer.TopTrumpsCards.Shuffle();
            opposingPlayer.TopTrumpsCards.Shuffle();

            //Get each respective player their next card
            var currentPlayerNextCard = currentPlayer.TopTrumpsCards.First();
            var opposingPlayerNextCard = opposingPlayer.TopTrumpsCards.First();

            //Send the current player their next card, this player gets true as they are playing the next move
            Clients.Client(currentPlayer.ConnectionId).moveDraw(currentPlayerNextCard, currentPlayer.TopTrumpsCards.Count, opposingPlayer.TopTrumpsCards.Count, true);

            //Send the opposing player their next card, this player gets false as they are not playing the next move
            Clients.Client(opposingPlayer.ConnectionId).moveDraw(opposingPlayerNextCard, opposingPlayer.TopTrumpsCards.Count, currentPlayer.TopTrumpsCards.Count, false);
        }

        private void GameWon(TopTrumpsGame topTrumpsGame, TopTrumpsPlayer winningPlayer, TopTrumpsPlayer losingPlayer)
        {
            //Set the time the game was won!
            topTrumpsGame.SetGameEndedTime();

            //Record the game in the database
            _gameBusinessLogic.RecordGameStats(new GameRecord
            {
                WinningPlayerId = winningPlayer.UserId,
                LosingPlayerId = losingPlayer.UserId,
                GameDuration = topTrumpsGame.GameEndedTime.Value - topTrumpsGame.GameStartedTime
            });

            //Notifies the winning player of the win
            Clients.Client(winningPlayer.ConnectionId).gameWon();

            //Notifies the losing player of their loss
            Clients.Client(losingPlayer.ConnectionId).gameLost();

            //Game is over so remove
            _topTrumpsGames.Remove(topTrumpsGame);
        }

        private TopTrumpsGame CreatePlayerAndAssignCards(Guid userId, TopTrumpsGame topTrumpsGame)
        {
            //Get the user from the database, if there isn't a user they are anonymous so make an object to use temporarily
            var user = _userBusinessLogic.GetUserById(userId) ?? new User
            {
                Id = userId,
                Username = "Anonymous"
            };

            //Create a new player object
            var newGamePlayer = new TopTrumpsPlayer(userId, user.Username, Context.ConnectionId);

            //Go through randomly assigning half the cards to the player
            for (int counter = 0; counter < topTrumpsGame.PlayerCardCount; counter++)
            {
                //Randomly get a card from the available cards and assign it to the player
                var topTrumpsCard = topTrumpsGame.AvailableTopTrumpsCards[_systemRandom.Next(topTrumpsGame.AvailableTopTrumpsCards.Count)];

                //Add the card to the current players deck
                newGamePlayer.TopTrumpsCards.Add(topTrumpsCard);

                //Remove the card from being available
                topTrumpsGame.AvailableTopTrumpsCards.Remove(topTrumpsCard);
            }

            //Shuffle the players cards so they are random
            newGamePlayer.TopTrumpsCards.Shuffle();

            //The player is now created correctly so add to game
            topTrumpsGame.TopTrumpsPlayers.Add(newGamePlayer);

            //Return the altered game
            return topTrumpsGame;
        }

        private void StartGameForUser(TopTrumpsGame topTrumpsGame, Guid userId)
        {
            //Get the newly created player
            var currentPlayer = GetGamePlayer(topTrumpsGame, userId);

            //Ensure that the player connecting is a valid person to be in this game
            if (currentPlayer == null)
            {
                //If they are then tell them they are an invalid user for the game
                Clients.Client(Context.ConnectionId).invalidGame();
            }
            else
            {
                //If lastPlayedUserId doesn't have a value, then assign one
                if (!topTrumpsGame.UserIdOfLastPlayed.HasValue)
                {
                    topTrumpsGame.UserIdOfLastPlayed = userId;
                }
                
                //If the amount of players in the game is 2 then the game is full so carry out extra actions to finalise the start of the match
                if (topTrumpsGame.TopTrumpsPlayers.Count == 2)
                {
                    //Get the opposing player to the one currently opposing the one who finalised the game
                    var opposingPlayer = GetGameOpposingPlayer(topTrumpsGame, userId);
                    
                    //If the current player Id is different to the current ConnectionId then reset the ConnectionId for that user
                    //and re-set up their game so they can continue
                    if (currentPlayer.ConnectionId != Context.ConnectionId)
                    {
                        //Reset the current users connection id as its changed
                        currentPlayer.ConnectionId = Context.ConnectionId;

                        //Set them as now being active
                        currentPlayer.PlayerInactive = false;

                        //Set the details for the reconnected user
                        Clients.Client(currentPlayer.ConnectionId).setOppositionsDetails(opposingPlayer.Username, 
                            currentPlayer.TopTrumpsCards.Count, opposingPlayer.TopTrumpsCards.Count);

                        //Notify the user of the reconnection
                        Clients.Client(opposingPlayer.ConnectionId).playerReconnected(currentPlayer.GetMessagePlayerName());
                    }
                    else
                    {
                        //Tell each of the respective players connected their opponents name
                        Clients.Client(currentPlayer.ConnectionId).setOppositionsDetails(opposingPlayer.Username,
                            currentPlayer.TopTrumpsCards.Count, opposingPlayer.TopTrumpsCards.Count);
                        Clients.Client(opposingPlayer.ConnectionId).setOppositionsDetails(currentPlayer.Username,
                            opposingPlayer.TopTrumpsCards.Count, currentPlayer.TopTrumpsCards.Count);

                        //Set the game started time
                        topTrumpsGame.SetGameStartedTime();
                    }
                }

                //Return the first card to the user along with if they are the first person playing a move
                Clients.Client(currentPlayer.ConnectionId).connectionEstablished(currentPlayer.TopTrumpsCards.First(), currentPlayer.Peeks,
                    topTrumpsGame.UserIdOfLastPlayed.Value == userId);
            }
        }

        private TopTrumpsPlayer GetGamePlayer(TopTrumpsGame topTrumpsGame, Guid userId)
        {
            //Get the player that matches the userId sent through
            return topTrumpsGame.TopTrumpsPlayers.SingleOrDefault(p => p.UserId == userId);
        }

        private TopTrumpsPlayer GetGameOpposingPlayer(TopTrumpsGame topTrumpsGame, Guid userId)
        {
            //Get the opposite player to the one that is sent through
            return topTrumpsGame.TopTrumpsPlayers.SingleOrDefault(p => p.UserId != userId);
        }

        private TopTrumpsGame GetTopTrumpsGame(Guid gameId)
        {
            //Get the current game that is going on
            return _topTrumpsGames.SingleOrDefault(g => g.GameId == gameId);
        }
    }
}
