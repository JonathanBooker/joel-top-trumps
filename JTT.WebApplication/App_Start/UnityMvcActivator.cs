using System.Linq;
using System.Web.Mvc;
using JTT.Interfaces;
using JTT.WebApplication.Resolver;
using JTT.WebApplication.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(JTT.WebApplication.App_Start.UnityWebActivator), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(JTT.WebApplication.App_Start.UnityWebActivator), "Shutdown")]

namespace JTT.WebApplication.App_Start
{
    /// <summary>Provides the bootstrapping for integrating Unity with ASP.NET MVC.</summary>
    public static class UnityWebActivator
    {
        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        public static void Start() 
        {
            var container = UnityConfig.GetConfiguredContainer();

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            
            GlobalHost.DependencyResolver = new SignalRUnityDependencyResolver(container);

            container.RegisterType<MatchmakingHub>(new InjectionFactory(CreateMatchmakingHub));
            container.RegisterType<TopTrumpsHub>(new InjectionFactory(CreateTopTrumpsHub));
        }

        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown()
        {
            var container = UnityConfig.GetConfiguredContainer();
            container.Dispose();
        }

        private static object CreateMatchmakingHub(IUnityContainer container)
        {
            var hub = new MatchmakingHub(container.Resolve<IDeckBusinessLogic>());
            return hub;
        }

        private static object CreateTopTrumpsHub(IUnityContainer container)
        {
            var hub = new TopTrumpsHub(container.Resolve<IDeckBusinessLogic>(), container.Resolve<IUserBusinessLogic>(),
                container.Resolve<IGameBusinessLogic>());
            return hub;
        }
    }
}