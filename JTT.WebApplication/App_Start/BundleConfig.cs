﻿using System.Web;
using System.Web.Optimization;

namespace JTT.WebApplication
{
    public class BundleConfig
    {
        public static string JqueryScriptBundle = "~/bundles/jquery";
        public static string JqueryValidateScriptBundle = "~/bundles/jqueryval";
        public static string ModernizrScriptBundle = "~/bundles/modernizr";
        public static string SignalRScriptBundle = "~/bundles/signalr";
        public static string HypeScriptBundle = "~/bundles/hype";
        public static string HypeGeneratedScriptBundle = "~/bundles/hype-generated";
        public static string TopTrumpsScripts = "~/bundles/scripts";

        public static string CssStylesBundle = "~/Content/css";
        
        public static void RegisterBundles(BundleCollection bundles)
        {
            //NOTE(JB): Force this just for HYPE to 
            BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle(JqueryScriptBundle).Include(
                "~/Scripts/jquery.color-{version}.js",
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle(JqueryValidateScriptBundle).Include(
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle(ModernizrScriptBundle).Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle(SignalRScriptBundle).Include(
                "~/Scripts/jquery.signalR*"));

            bundles.Add(new ScriptBundle(HypeScriptBundle).Include(
                "~/Scripts/hype.js"));

            bundles.Add(new ScriptBundle(HypeGeneratedScriptBundle).Include(
                "~/Scripts/loading_hype_generated_script.js"));

            bundles.Add(new ScriptBundle(TopTrumpsScripts).Include(
                "~/Scripts/top-trumps-play-game.js"));

            bundles.Add(new StyleBundle(CssStylesBundle).Include(
                "~/Content/reset.css",
                "~/Content/loading.css",
                "~/Content/site.css",
                "~/Content/font-awesome-4.3-2.0/css/font-awesome.min.css"));
                
        }
    }
}
