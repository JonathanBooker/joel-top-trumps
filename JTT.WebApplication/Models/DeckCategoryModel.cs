﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTT.WebApplication.Models
{
    public class DeckCategoryModel
    {
        public Guid Id { get; set; }
        public string CategoryName { get; set; }
        public int Index { get; set; }
    }
}