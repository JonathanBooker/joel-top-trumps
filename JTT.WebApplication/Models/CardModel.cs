﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTT.WebApplication.Models
{
    public class CardModel
    {
        public string CardName { get; set; }
        public IEnumerable<CardCategoryModel> CardCategories { get; set; }
    }
}