﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models.Game
{
    public class TopTrumpsGame
    {
        public Guid GameId { get; set; }
        public List<TopTrumpsPlayer> TopTrumpsPlayers { get; set; }
        public List<TopTrumpsCard> AvailableTopTrumpsCards { get; set; }
        public int PlayerCardCount { get; set; }
        public Guid? UserIdOfLastPlayed { get; set; }
        public DateTime GameStartedTime { get; set; }
        public DateTime? GameEndedTime { get; set; }

        public TopTrumpsGame(Guid gameId, List<TopTrumpsCard> availableTopTrumpsCards)
        {
            GameId = gameId;
            TopTrumpsPlayers = new List<TopTrumpsPlayer>();
            AvailableTopTrumpsCards = availableTopTrumpsCards;
            PlayerCardCount = availableTopTrumpsCards.Count / 2;
        }

        public void SetGameStartedTime()
        {
            GameStartedTime = DateTime.Now;
        }
        
        public void SetGameEndedTime()
        {
            GameEndedTime = DateTime.Now;
        }
    }
}
