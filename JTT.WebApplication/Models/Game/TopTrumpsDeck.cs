﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models.Game
{
    public class TopTrumpsDeck
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<TopTrumpsCard> TopTrumpsCards { get; set; }

        public TopTrumpsDeck()
        {
            TopTrumpsCards = new List<TopTrumpsCard>();
        }

        public TopTrumpsDeck(Guid id, string name, List<TopTrumpsCard> topTrumpsCards)
        {
            Id = id;
            Name = name;
            TopTrumpsCards = topTrumpsCards;
        }
    }
}
