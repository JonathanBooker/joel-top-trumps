﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTT.WebApplication.Models.Game
{
    public class TopTrumpsCategory
    {
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public bool LowestWins { get; set; }

        public TopTrumpsCategory() { }

        public TopTrumpsCategory(Guid categoryId, string name, int value)
        {
            CategoryId = categoryId;
            Name = name;
            Value = value;
            LowestWins = false;
        }
    }
}