﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models.Game
{
    public class TopTrumpsPlayer
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public List<TopTrumpsCard> TopTrumpsCards { get; set; }
        public string ConnectionId { get; set; }
        public int Peeks { get; set; }
        public bool PlayerInactive { get; set; }

        public TopTrumpsPlayer(Guid userId, string username, string connectionId)
        {
            UserId = userId;
            Username = username;
            ConnectionId = connectionId;
            TopTrumpsCards = new List<TopTrumpsCard>();
            Peeks = 1;
            PlayerInactive = false;
        }

        public string GetMessagePlayerName()
        {
            var partialUserId = UserId.ToString().Substring(0, 5);
            return string.Format("{0} ({1}) - {2}", Username, partialUserId, DateTime.Now.ToShortTimeString());
        }

        public bool DoesPlayerHavePeeks()
        {
            return Peeks > 0;
        }

        public void DecrementPlayerPeeks()
        {
            Peeks -= 1;
        }
    }
}
