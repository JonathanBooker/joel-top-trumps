﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models.Game
{
    public class TopTrumpsCard
    {
        public Guid CardId { get; set; }
        public string Name { get; set; }
        public List<TopTrumpsCategory> TopTrumpsCategories { get; set; }
        public string ImageAddress { get; set; }

        public TopTrumpsCard()
        {
            TopTrumpsCategories = new List<TopTrumpsCategory>();
            ImageAddress = null;
        }

        public TopTrumpsCard(Guid cardId, string name, List<TopTrumpsCategory> topTrumpsCategories)
        {
            CardId = cardId;
            Name = name;
            TopTrumpsCategories = topTrumpsCategories;
        }

        public TopTrumpsCard(Guid cardId, string name, List<TopTrumpsCategory> topTrumpsCategories, string imageAddress)
        {
            CardId = cardId;
            Name = name;
            TopTrumpsCategories = topTrumpsCategories;
            ImageAddress = imageAddress;
        }
    }
}
