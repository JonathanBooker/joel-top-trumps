﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTT.WebApplication.Models
{
    public class CardCategoryModel
    {
        public int Index { get; set; }
        public int Value { get; set; }
        public Guid DeckCategoryId { get; set; }
    }
}