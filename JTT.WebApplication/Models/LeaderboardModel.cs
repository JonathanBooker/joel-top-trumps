﻿using System.Collections.Generic;
using JTT.Domain.Core;
using JTT.Domain.DTO;

namespace JTT.WebApplication.Models
{
    public class LeaderboardModel
    {
        public List<LeaderboardPlayer> LeaderboardPlayers { get; set; }
        public List<Deck> Decks { get; set; }

        public LeaderboardModel()
        {
            LeaderboardPlayers = new List<LeaderboardPlayer>();
            Decks = new List<Deck>();
        }
    }
}