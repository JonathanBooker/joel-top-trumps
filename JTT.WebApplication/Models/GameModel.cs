﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models
{
    public class GameModel
    {
        public Guid GameId { get; set; }
        public Guid DeckId { get; set; }
        public Guid UserId { get; set; }
        public string DeckName { get; set; }
    }
}
