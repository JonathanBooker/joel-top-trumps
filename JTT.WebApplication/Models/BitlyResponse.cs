﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JsonFx.Json;

namespace JTT.WebApplication.Models
{
    public class LinkSave
    {
        [JsonName("link")]
        public string Link { get; set; }
        [JsonName("aggregate_link")]
        public string AggregateLink { get; set; }
        [JsonName("long_url")]
        public string LongUrl { get; set; }
        [JsonName("new_link")]
        public string NewLink { get; set; }
    }

    public class BitlyResponse
    {
        [JsonName("status_code")]
        public string StatusCode { get; set; }
        [JsonName("data")]
        public Data Data { get; set; }
    }

    public class Data
    {
        [JsonName("link_save")]
        public LinkSave LinkSave { get; set; }
    }
}