﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace JTT.WebApplication.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string RedirectURL { get; set; }

        public bool LoginSuccess { get; set; }

        public LoginModel()
        {
            LoginSuccess = true;
        }
    }
}
