﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JTT.WebApplication.Models
{
    public class DeckModel
    {
        public Guid Id { get; set; } 
        public string DeckName { get; set; }
        public IEnumerable<DeckCategoryModel> DeckCategories { get; set; }
        public IEnumerable<CardModel> Cards { get; set; }
    }

}