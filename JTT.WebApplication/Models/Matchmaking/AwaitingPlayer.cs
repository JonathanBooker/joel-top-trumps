﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.WebApplication.Models.Matchmaking
{
    public class AwaitingPlayer
    {
        public Guid UserId { get; set; }
        public Guid? DeckId { get; set; }
        public DateTime TimeAdded { get; set; }
        public string ConnectionId { get; set; }
    }
}
