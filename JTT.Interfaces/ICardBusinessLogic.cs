﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.DTO;

namespace JTT.Interfaces
{
    public interface ICardBusinessLogic
    {
        Guid CreateCard(Card domainObject);
        List<Card> GetCardsForDeck(Guid deckId);
        Card GetFirstCardForDeck(Guid deckId);
    }
}
