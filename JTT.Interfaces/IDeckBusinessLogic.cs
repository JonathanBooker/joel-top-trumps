﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.DTO;

namespace JTT.Interfaces
{
    public interface IDeckBusinessLogic
    {
        Deck GetById(Guid id);
        List<Deck> GetAllDecks();
        Guid CreateDeck(Deck domainObject);
    }
}
