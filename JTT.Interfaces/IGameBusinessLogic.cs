﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.Core;

namespace JTT.Interfaces
{
    public interface IGameBusinessLogic
    {
        bool RecordGameStats(GameRecord gameRecord);
        List<LeaderboardPlayer> GetRankedUsers();
        List<LeaderboardPlayer> GetSearchUsers(string searchText);
    }
}
