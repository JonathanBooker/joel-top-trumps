﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.DTO;

namespace JTT.Interfaces
{
    public interface IDeckCategoryBusinessLogic
    {
        Guid CreateDeckCategory(DeckCategory domainObject);

        List<DeckCategory> GetDeckCategoriesForDeck(Guid deckId);
    }
}
