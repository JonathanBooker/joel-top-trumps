﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.DTO;

namespace JTT.Interfaces
{
    public interface IUserBusinessLogic
    {
        User GetUserById(Guid userId);
    }
}
