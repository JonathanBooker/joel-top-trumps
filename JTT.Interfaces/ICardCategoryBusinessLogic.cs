﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.DTO;

namespace JTT.Interfaces
{
    public interface ICardCategoryBusinessLogic
    {
        List<CardCategory> CreateCardCategory(List<CardCategory> domainObject);
    }
}
