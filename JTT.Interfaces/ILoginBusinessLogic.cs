﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.Domain.Core;
using JTT.Domain.DTO;
using JTT.Domain.Enums;

namespace JTT.Interfaces
{
    public interface ILoginBusinessLogic
    {
        bool ValidateUser(string username, string password);
        ApplicationUser LoginUser(string username, string password);
        CreateUserResult CreateUser(User user, string password);
    }
}
