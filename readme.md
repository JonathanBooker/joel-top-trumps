# **Top Trumps Application** #

## **Jonathan Booker, Noah Knudsen & Ste Prescott** ##
### **Cloud Applications** ###

To run this application you will need:

* Visual Studio 2013
* Latest Copy of 'master'
* Azure Table Storage Emulator installed and running




To run the application:

1. Open 'Joel Top Trumps.sln'
2. Right click on Solution within 'Solution Explorer' and click 'Enable NuGet Package Restore'
3. Click 'Build' and then click 'Rebuild Solution' (This will download all necessary NuGet packages
4. Upon successful build, set 'JTT.WebApplication' as the start-up project and then press 'Crtl+F5;
5. Allow to startup


Enjoy!