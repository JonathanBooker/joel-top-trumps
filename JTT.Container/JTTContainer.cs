﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.BusinessLogicEntityFramework;
using JTT.Interfaces;
using JTT.UnitOfWorkEf;
using Microsoft.Practices.Unity;

namespace JTT.Container
{
    public static class JttContainer
    {
        /// <summary>
        /// A publically facing container which registers all the implementations to interfaces
        /// </summary>
        public static UnityContainer UnityContainer
        {
            get
            {
                if (_unityContainer == null)
                {
                    Register();
                }

                return _unityContainer;
            }
        }

        /// <summary>
        /// A singleton container in which the implementations are held within
        /// </summary>
        private static UnityContainer _unityContainer;

        /// <summary>
        /// Gets back a concrete implementation for an interface
        /// </summary>
        /// <typeparam name="T">The interface we are getting a concrete implementation for</typeparam>
        /// <returns>A concrete implementation</returns>
        public static T GetInstance<T>()
        {
            if (_unityContainer == null)
            {
                Register();
            }

            return _unityContainer.Resolve<T>();
        }

        /// <summary>
        /// Sets the interfaces to concrete implementations
        /// </summary>
        private static void Register()
        {
            //Registers the ability to use IOC
            _unityContainer = new UnityContainer();

            //Sets the concrete implementations to the interfaces for a relational database
            _unityContainer.RegisterType<ICardBusinessLogic, CardBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<IDeckCategoryBusinessLogic, DeckCategoryBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<ICardCategoryBusinessLogic, CardCategoryBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<ILoginBusinessLogic, LoginBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<IUserBusinessLogic, UserBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<IDeckBusinessLogic, DeckBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<IGameBusinessLogic, GameBusinessLogic>(new HierarchicalLifetimeManager());
            _unityContainer.RegisterType<IUnitOfWork, UnitOfWork>(new ContainerControlledLifetimeManager());

            //Sets the concrete implementations to the interfaces for Azure Document DB
            //_unityContainer.RegisterType<ILoginBusinessLogic, BusinessLogicDocumentDb.LoginBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<IUserBusinessLogic, BusinessLogicDocumentDb.UserBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<IDeckBusinessLogic, BusinessLogicDocumentDb.DeckBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<IDeckCategoryBusinessLogic, BusinessLogicDocumentDb.DeckCategoryBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<ICardBusinessLogic, BusinessLogicDocumentDb.CardBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<ICardCategoryBusinessLogic, BusinessLogicDocumentDb.CardCategoryBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<IGameBusinessLogic, BusinessLogicDocumentDb.GameBusinessLogic>(new HierarchicalLifetimeManager());
            //_unityContainer.RegisterType<IUnitOfWork, UnitOfWorkDocumentDb.UnitOfWork>(new ContainerControlledLifetimeManager());
        }
    }
}
