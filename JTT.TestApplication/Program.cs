﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.UnitOfWorkDocumentDb;
using JTT.UnitOfWorkDocumentDb.Classes;

namespace JTT.TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var identifier = Guid.NewGuid();

                unitOfWork.Add(new Deck
                {
                    Id = identifier,
                    Name = Guid.NewGuid().ToString(),
                    Cards = new List<Card>
                    {
                        new Card
                        {
                            Name = "Test",
                            CardId = Guid.NewGuid()
                        }
                    }
                });

                var test1 = unitOfWork.GetById<Deck>(identifier);
                Console.WriteLine(test1.Name);
                test1.Name = "Hi";
                unitOfWork.Update(test1);
                var test2 = unitOfWork.GetById<Deck>(identifier);
                Console.WriteLine(test1.Name);

                Console.ReadKey();
            }
            //Console.WriteLine("Done");
        }
    }
}
