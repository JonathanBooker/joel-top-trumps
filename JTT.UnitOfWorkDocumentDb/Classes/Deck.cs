﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class Deck
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<DeckCategory> DeckCategories { get; set; }
        public List<Card> Cards { get; set; }
    }
}
