﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class CardCategory
    {
        public Guid Id { get; set; }
        public Int32 Score { get; set; }
        public Guid DeckCategoryId { get; set; }
        public string DeckCategoryName { get; set; }
        public Guid CardId { get; set; }
        public Card Card { get; set; }
    }
}
