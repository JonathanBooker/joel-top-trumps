﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class DeckCategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool HighestWins { get; set; }
    }
}
