﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class Card
    {
        public Guid CardId { get; set; }
        public string Name { get; set; }
        public Guid BlobImageId { get; set; }
        public List<CardCategory> CardCategories { get; set; }
    }
}
