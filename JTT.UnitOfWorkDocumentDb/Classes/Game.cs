﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class Game
    {
        public Guid Id { get; set; }
        public string DeckName { get; set; }
        public long GameDuration { get; set; }
        public List<User> Users { get; set; }
        public Guid WinningUserId { get; set; }
    }
}
