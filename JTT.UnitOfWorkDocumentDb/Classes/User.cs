﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace JTT.UnitOfWorkDocumentDb.Classes
{
    public class User
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Forname { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public List<Game> Games { get; set; }

        public User()
        {
            Games = new List<Game>();
        }
    }
}
