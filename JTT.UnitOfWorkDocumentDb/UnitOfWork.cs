﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JTT.Interfaces;
using JTT.SharedComponents;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;

namespace JTT.UnitOfWorkDocumentDb
{
    public class UnitOfWork : IUnitOfWork
    {
        private static readonly string EndpointUrl = ConfigurationUtilities.GetConfigurationSetting("EndPointUrl");
        private static readonly string AuthorizationKey = ConfigurationUtilities.GetConfigurationSetting("AuthorizationKey");

        private static readonly string DatabaseName = ConfigurationUtilities.GetConfigurationSetting("DocumentDbDatabaseName");

        private readonly DocumentClient _documentClient;
        private Database _database;

        public UnitOfWork()
        {
            _documentClient = new DocumentClient(new Uri(EndpointUrl), AuthorizationKey);
            _database = _documentClient.CreateDatabaseQuery().Where(db => db.Id == DatabaseName).AsEnumerable().FirstOrDefault();
            CreateOrGetDatabase().Wait();
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            var collection = CreateOrGetCollection<T>().Result;

            return GetDocumentItems<T>(collection).AsQueryable();
        }

        public T GetById<T>(object id) where T : class
        {
            var collection = CreateOrGetCollection<T>().Result;

            var item = _documentClient.CreateDocumentQuery<T>(collection.DocumentsLink,
                "SELECT * FROM " + typeof(T).Name + " i WHERE i.id = '" + id + "'")
                .AsEnumerable()
                .FirstOrDefault();

            return item;
        }

        public T Add<T>(T entity) where T : class
        {
            var collection = CreateOrGetCollection<T>().Result;

            _documentClient.CreateDocumentAsync(collection.DocumentsLink, entity);

            return entity;
        }

        public ICollection<T> AddRange<T>(ICollection<T> entity) where T : class
        {
            foreach (var item in entity)
            {
                Add(item);
            }

            return entity;
        }

        public void Remove<T>(T entity) where T : class
        {
            var collection = CreateOrGetCollection<T>().Result;
            
            string dynamicEntityId = ((dynamic) entity).Id.ToString();

            var item = _documentClient.CreateDocumentQuery(collection.DocumentsLink,
                "SELECT * FROM " + typeof(T).Name + " i WHERE i.id = '" + dynamicEntityId + "'")
                .AsEnumerable()
                .FirstOrDefault();

            if (item != null)
            {
                _documentClient.DeleteDocumentAsync(item._self);
            }
        }

        public void Update<T>(T entity) where T : class
        {
            var collection = CreateOrGetCollection<T>().Result;

            _documentClient.ReplaceDocumentAsync(collection.DocumentsLink, entity);
        }

        public bool HasBeenModified<T>(T entity) where T : class
        {
            throw new Exception("Not possible with document db");
        }

        public void SaveChanges()
        {
            //Not needed with document
        }

        public void Dispose()
        {
            _documentClient.Dispose();
        }

        private async Task CreateOrGetDatabase()
        {
            if (_database == null)
            {
                // Create a database
                _database = await _documentClient.CreateDatabaseAsync(
                    new Database { Id = DatabaseName });
            }
        }

        private async Task<DocumentCollection> CreateOrGetCollection<T>() where T : class
        {
            var typeName = typeof(T).Name;
            var collection = _documentClient.CreateDocumentCollectionQuery(_database.CollectionsLink).Where(c => c.Id == typeName).AsEnumerable().FirstOrDefault();

            if (collection == null)
            {
                collection = await _documentClient.CreateDocumentCollectionAsync(_database.CollectionsLink,
                    new DocumentCollection { Id = typeName },
                    new RequestOptions { OfferType = "S1" });
            }

            return collection;
        }

        private IEnumerable<T> GetDocumentItems<T>(DocumentCollection documentCollection) where T : class
        {
            return _documentClient.CreateDocumentQuery<T>(documentCollection.DocumentsLink).AsEnumerable().ToList();
        }
    }
}
