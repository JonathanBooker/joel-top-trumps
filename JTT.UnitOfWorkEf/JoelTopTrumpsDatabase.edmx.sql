
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/19/2015 17:50:50
-- Generated from EDMX file: C:\Users\Noah\Documents\joel-top-trumps\JTT.UnitOfWorkEf\JoelTopTrumpsDatabase.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Joel Top Trumps];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_DeckCard]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cards] DROP CONSTRAINT [FK_DeckCard];
GO
IF OBJECT_ID(N'[dbo].[FK_DeckDeckCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DeckCategories] DROP CONSTRAINT [FK_DeckDeckCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_DeckCategoryCardCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardCategories] DROP CONSTRAINT [FK_DeckCategoryCardCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_CardCardCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CardCategories] DROP CONSTRAINT [FK_CardCardCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_DeckGame]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_DeckGame];
GO
IF OBJECT_ID(N'[dbo].[FK_GameUser_Game]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameUser] DROP CONSTRAINT [FK_GameUser_Game];
GO
IF OBJECT_ID(N'[dbo].[FK_GameUser_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GameUser] DROP CONSTRAINT [FK_GameUser_User];
GO
IF OBJECT_ID(N'[dbo].[FK_GameUser1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_GameUser1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Cards]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cards];
GO
IF OBJECT_ID(N'[dbo].[Decks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Decks];
GO
IF OBJECT_ID(N'[dbo].[DeckCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeckCategories];
GO
IF OBJECT_ID(N'[dbo].[CardCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CardCategories];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Games]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Games];
GO
IF OBJECT_ID(N'[dbo].[GameUser]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameUser];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Cards'
CREATE TABLE [dbo].[Cards] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [DeckId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Decks'
CREATE TABLE [dbo].[Decks] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DeckCategories'
CREATE TABLE [dbo].[DeckCategories] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [DeckId] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'CardCategories'
CREATE TABLE [dbo].[CardCategories] (
    [Score] nvarchar(max)  NOT NULL,
    [DeckCategoryId] uniqueidentifier  NOT NULL,
    [CardId] uniqueidentifier  NOT NULL,
    [Id] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] uniqueidentifier  NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Forename] nvarchar(max)  NOT NULL,
    [Surname] nvarchar(max)  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Games'
CREATE TABLE [dbo].[Games] (
    [Id] uniqueidentifier  NOT NULL,
    [DeckId] uniqueidentifier  NOT NULL,
    [GameDuration] bigint  NOT NULL,
    [WinningUser_Id] uniqueidentifier  NULL
);
GO

-- Creating table 'GameUser'
CREATE TABLE [dbo].[GameUser] (
    [Games_Id] uniqueidentifier  NOT NULL,
    [Users_Id] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Cards'
ALTER TABLE [dbo].[Cards]
ADD CONSTRAINT [PK_Cards]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Decks'
ALTER TABLE [dbo].[Decks]
ADD CONSTRAINT [PK_Decks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeckCategories'
ALTER TABLE [dbo].[DeckCategories]
ADD CONSTRAINT [PK_DeckCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CardCategories'
ALTER TABLE [dbo].[CardCategories]
ADD CONSTRAINT [PK_CardCategories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [PK_Games]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Games_Id], [Users_Id] in table 'GameUser'
ALTER TABLE [dbo].[GameUser]
ADD CONSTRAINT [PK_GameUser]
    PRIMARY KEY CLUSTERED ([Games_Id], [Users_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DeckId] in table 'Cards'
ALTER TABLE [dbo].[Cards]
ADD CONSTRAINT [FK_DeckCard]
    FOREIGN KEY ([DeckId])
    REFERENCES [dbo].[Decks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeckCard'
CREATE INDEX [IX_FK_DeckCard]
ON [dbo].[Cards]
    ([DeckId]);
GO

-- Creating foreign key on [DeckId] in table 'DeckCategories'
ALTER TABLE [dbo].[DeckCategories]
ADD CONSTRAINT [FK_DeckDeckCategory]
    FOREIGN KEY ([DeckId])
    REFERENCES [dbo].[Decks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeckDeckCategory'
CREATE INDEX [IX_FK_DeckDeckCategory]
ON [dbo].[DeckCategories]
    ([DeckId]);
GO

-- Creating foreign key on [DeckCategoryId] in table 'CardCategories'
ALTER TABLE [dbo].[CardCategories]
ADD CONSTRAINT [FK_DeckCategoryCardCategory]
    FOREIGN KEY ([DeckCategoryId])
    REFERENCES [dbo].[DeckCategories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeckCategoryCardCategory'
CREATE INDEX [IX_FK_DeckCategoryCardCategory]
ON [dbo].[CardCategories]
    ([DeckCategoryId]);
GO

-- Creating foreign key on [CardId] in table 'CardCategories'
ALTER TABLE [dbo].[CardCategories]
ADD CONSTRAINT [FK_CardCardCategory]
    FOREIGN KEY ([CardId])
    REFERENCES [dbo].[Cards]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CardCardCategory'
CREATE INDEX [IX_FK_CardCardCategory]
ON [dbo].[CardCategories]
    ([CardId]);
GO

-- Creating foreign key on [DeckId] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_DeckGame]
    FOREIGN KEY ([DeckId])
    REFERENCES [dbo].[Decks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeckGame'
CREATE INDEX [IX_FK_DeckGame]
ON [dbo].[Games]
    ([DeckId]);
GO

-- Creating foreign key on [Games_Id] in table 'GameUser'
ALTER TABLE [dbo].[GameUser]
ADD CONSTRAINT [FK_GameUser_Game]
    FOREIGN KEY ([Games_Id])
    REFERENCES [dbo].[Games]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'GameUser'
ALTER TABLE [dbo].[GameUser]
ADD CONSTRAINT [FK_GameUser_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameUser_User'
CREATE INDEX [IX_FK_GameUser_User]
ON [dbo].[GameUser]
    ([Users_Id]);
GO

-- Creating foreign key on [WinningUser_Id] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_GameUser1]
    FOREIGN KEY ([WinningUser_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameUser1'
CREATE INDEX [IX_FK_GameUser1]
ON [dbo].[Games]
    ([WinningUser_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------