﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using JTT.Interfaces;

namespace JTT.UnitOfWorkEf
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JoelTopTrumpsDatabaseContainer _mscGroupProjectEntities;

        public UnitOfWork()
        {
            _mscGroupProjectEntities = new JoelTopTrumpsDatabaseContainer();

            //For Debug Purposes
            //_mscGroupProjectEntities.Database.Log = s => Debug.Write(s);
        }

        public T GetById<T>(object id) where T : class
        {
            return _mscGroupProjectEntities.Set<T>().Find(id);
        }

        public T Add<T>(T entity) where T : class
        {
            return _mscGroupProjectEntities.Set<T>().Add(entity);
        }

        public ICollection<T> AddRange<T>(ICollection<T> entity) where T : class
        {
            return _mscGroupProjectEntities.Set<T>().AddRange(entity).ToList();
        }

        public void Update<T>(T entity) where T : class
        {
            _mscGroupProjectEntities.Set<T>().Attach(entity);
            _mscGroupProjectEntities.Entry(entity).State = EntityState.Modified;
        }

        public void Remove<T>(T entity) where T : class
        {
            _mscGroupProjectEntities.Set<T>().Remove(entity);
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return _mscGroupProjectEntities.Set<T>();
        }

        public bool HasBeenModified<T>(T entity) where T : class
        {
            return _mscGroupProjectEntities.Entry(entity).State == EntityState.Modified;
        }

        public void SaveChanges()
        {
            _mscGroupProjectEntities.SaveChanges();
        }

        public void Dispose()
        {
            _mscGroupProjectEntities.Dispose();
        }
    }
}
