﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JTT.UnitOfWorkEf
{
    public partial class User
    {
        public long TotalGameTimeTicks
        {
            get
            {
                return Games.Sum(g => g.GameDuration) + WonGames.Sum(wg => wg.GameDuration);
            }
        }

        public TimeSpan TotalGameTime
        {
            get
            {
                return new TimeSpan(TotalGameTimeTicks);
            }
        }

        public TimeSpan AverageGameTime
        {
            get
            {
                return TotalGames > 0 ? new TimeSpan(TotalGameTimeTicks / TotalGames) : new TimeSpan();
            }
        }

        public int TotalGames
        {
            get
            {
                return Games.Count + WonGames.Count;
            }
        }
    }
}
