﻿using System;
using System.Collections.Generic;
using System.Linq;
using JTT.SharedComponents;
using JTT.UnitOfWorkEf;

namespace JTT.DataPopulator
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                //The users
                var user1 = new User
                {
                    Forename = "Jonny",
                    Surname = "Booker",
                    Username = "JonnyBooker",
                    Password = EncryptionUtilities.Encrypt("Password"),
                    Id = Guid.NewGuid(),
                    PhoneNumber = "447840660968"
                };
                var user2 = new User
                {
                    Forename = "Ste",
                    Surname = "Prescott",
                    Username = "StePrescott",
                    Password = EncryptionUtilities.Encrypt("Password"),
                    Id = Guid.NewGuid(),
                    PhoneNumber = "447985379666"
                };
                var user3 = new User
                {
                    Forename = "Noah",
                    Surname = "Knudsen",
                    Username = "NoahKnudsen",
                    Password = EncryptionUtilities.Encrypt("Password"),
                    Id = Guid.NewGuid()
                };
                var user4 = new User
                {
                    Forename = "Joe",
                    Surname = "Fletcher",
                    Username = "JoeFletcher",
                    Password = EncryptionUtilities.Encrypt("Password"),
                    Id = Guid.NewGuid()
                };
                var user5 = new User
                {
                    Forename = "Tom",
                    Surname = "Windowson",
                    Username = "TomWindowson",
                    Password = EncryptionUtilities.Encrypt("Password"),
                    Id = Guid.NewGuid()
                };

                //user1.Games.Add(new Game
                //{
                //    Id = Guid.NewGuid(),
                //    WinningUser = user1,
                //    GameDuration = new TimeSpan(0, 0, 5, 35).Ticks,
                //    Users = new Collection<User>(new List<User> { user2 })
                //});

                //Add the users
                unitOfWork.Add(user1);
                unitOfWork.Add(user2);
                unitOfWork.Add(user3);
                unitOfWork.Add(user4);
                unitOfWork.Add(user5);

                //Save the changes
                unitOfWork.SaveChanges();

                Console.WriteLine("Done");
                Console.ReadKey();
            }
        }
    }
}
